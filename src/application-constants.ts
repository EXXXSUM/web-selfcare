export const PLANTYPE = {
  'STANDARD': 'standard',
  'PREMIUM': 'premium',
  'CUSTOM': 'custom',
  'CUSTOMIZEPLANOFFER': 'customizeplanoffer'
};

export const QUOTATYPE = {
  'DATA': 'data',
  'VOICE': 'voice',
  'SMS': 'sms'
};

export const PRODUCTGROUP = {
  'ADDON': 'addon'
};

export const PERSISTANCEKEY = {
  CURRENTPLAN: 'currentplan',
  ACCESSTOKEN: 'accessToken',
  MSISDN: 'msisdn',
  USERNAME: 'username',
  USER: 'user',
  USERADDON: 'useraddons',
  PROVISIONEDQUOTA: 'provisionedQuota',
  AVAILABLEQUOTA: 'availableQuota',
  MAINBALANCE: 'mainBalance'
};

export const ONBOARDPERSISTANCEKEY = {
  MSISDN: 'msisdn',
  ENABLEREGISTER: 'enableRegister',
  ENABLEDELIVERY: 'enableDelivery',
  ENABLEADDON : 'enableAddOn',
  USERADDON: 'useraddons',
  SELECTEDADDONS: 'selectedaddons',
  CURRENTPLAN: 'currentplan',
  CUSTOMOFFER: 'customoffer',
  PREMIUM: 'premium',
  STANDARD: 'standard',
  INVENTORY: 'inventory',
  PINCODE: 'pincode',
  SELECTEDDEVICE: 'selecteddevice',
  ENABLECHECKOUT : 'enableCheckout',
  REGISTERDATA : 'registerData',
  CUSTOMPLANPRICE : 'customPrice',
  CUSTOMPLANINDEX : 'customPlanIndex'
};
