import {ErrorHandler, Injectable} from '@angular/core';
import {HttpErrorResponse} from '@angular/common/http';

declare const alertify;
@Injectable()
export class ErrorsHandlerService implements ErrorHandler {

  constructor() { }

  handleError(error: Error): void {
    if (error instanceof HttpErrorResponse) {
      // Server or connection error happened
      if (!navigator.onLine) {
        alertify.error('No internet connection');
      } else {
        if (error.status === 400) {
          /* reissue token */
          alertify.error('Invalid request please try again.');
        } else if (error.status === 401) {
          /* reissue token */
          alertify.error('Session expired');
        }
      }
    } else {
      alertify.error('Something went wrong please try again');
      console.log('Something went wrong please try again', error);
    }

  }

}
