import {EventEmitter, Injectable} from '@angular/core';
import {PersistenceService, StorageType} from 'angular-persistence';
import {EventEnum} from './enum/EventEnum';
import {PERSISTANCEKEY} from '../application-constants';

@Injectable()
export class EventListenerService {
  public balanceEvent: EventEmitter<any> = new EventEmitter<any>();
  public profileEvent: EventEmitter<any> = new EventEmitter<any>();
  public customerDataChangesEvent: EventEmitter<any> = new EventEmitter<any>();

  constructor(private persistenceService: PersistenceService) {
  }
  customerDataChanges(data, voice, sms) {
    const customerDataObj = {'data': data, 'voice': voice, 'sms': sms};
    this.customerDataChangesEvent.emit(customerDataObj);
  }
  addToMainBalance(value) {
    const res = this.persistenceService.get(PERSISTANCEKEY.MAINBALANCE, StorageType.SESSION);
    res.mainBalance.unit = res.mainBalance.unit + value;
    this.persistenceService.set(PERSISTANCEKEY.MAINBALANCE, res, {type: StorageType.SESSION});
    this.balanceEvent.emit(EventEnum.mainBalanceUpdated);
  }

  removeFromMainBalance(value) {
    const res = this.persistenceService.get(PERSISTANCEKEY.MAINBALANCE, StorageType.SESSION);
    res.mainBalance.unit = res.mainBalance.unit - value;
    this.persistenceService.set(PERSISTANCEKEY.MAINBALANCE, res, {type: StorageType.SESSION});
    this.balanceEvent.emit(EventEnum.mainBalanceUpdated);
  }

  addData(data, price) {
    const availableQuota = this.persistenceService.get(PERSISTANCEKEY.AVAILABLEQUOTA, StorageType.SESSION);
    const provisionedQuota = this.persistenceService.get(PERSISTANCEKEY.PROVISIONEDQUOTA, StorageType.SESSION);

    availableQuota.data = availableQuota.data + data;
    if (provisionedQuota.data >= 0) {
      provisionedQuota.data = provisionedQuota.data + data;
    }

    this.persistenceService.set(PERSISTANCEKEY.AVAILABLEQUOTA, availableQuota, {type: StorageType.SESSION});
    this.persistenceService.set(PERSISTANCEKEY.PROVISIONEDQUOTA, provisionedQuota, {type: StorageType.SESSION});
    this.balanceEvent.emit(EventEnum.dataUpdated);
    this.removeFromMainBalance(price);
  }

  removeData(data) {
    const availableQuota = this.persistenceService.get(PERSISTANCEKEY.AVAILABLEQUOTA, StorageType.SESSION);
    availableQuota.data = availableQuota.data - data;
    this.persistenceService.set(PERSISTANCEKEY.AVAILABLEQUOTA, availableQuota, {type: StorageType.SESSION});
    this.balanceEvent.emit(EventEnum.dataUpdated);
  }

  addMinutes(voice, price) {
    const availableQuota = this.persistenceService.get(PERSISTANCEKEY.AVAILABLEQUOTA, StorageType.SESSION);
    const provisionedQuota = this.persistenceService.get(PERSISTANCEKEY.PROVISIONEDQUOTA, StorageType.SESSION);

    availableQuota.voice = availableQuota.voice + voice;
    if (provisionedQuota.voice >= 0) {
      provisionedQuota.voice = provisionedQuota.voice + voice;
    }

    this.persistenceService.set(PERSISTANCEKEY.AVAILABLEQUOTA, availableQuota, {type: StorageType.SESSION});
    this.persistenceService.set(PERSISTANCEKEY.PROVISIONEDQUOTA, provisionedQuota, {type: StorageType.SESSION});

    this.balanceEvent.emit(EventEnum.voiceUpdated);
    this.removeFromMainBalance(price);
  }

  removeMinutes(voice) {
    const availableQuota = this.persistenceService.get(PERSISTANCEKEY.AVAILABLEQUOTA, StorageType.SESSION);
    this.persistenceService.set(PERSISTANCEKEY.AVAILABLEQUOTA, availableQuota, {type: StorageType.SESSION});
    this.balanceEvent.emit(EventEnum.voiceUpdated);
  }

  addSMS(sms, price) {
    const availableQuota = this.persistenceService.get(PERSISTANCEKEY.AVAILABLEQUOTA, StorageType.SESSION);
    const provisionedQuota = this.persistenceService.get(PERSISTANCEKEY.PROVISIONEDQUOTA, StorageType.SESSION);

    availableQuota.sms = availableQuota.sms + sms;
    if (provisionedQuota.sms >= 0) {
      provisionedQuota.sms = provisionedQuota.sms + sms;
    }
    this.persistenceService.set(PERSISTANCEKEY.AVAILABLEQUOTA, availableQuota, {type: StorageType.SESSION});
    this.persistenceService.set(PERSISTANCEKEY.PROVISIONEDQUOTA, provisionedQuota, {type: StorageType.SESSION});

    this.balanceEvent.emit(EventEnum.smsUpdated);
    this.removeFromMainBalance(price);
  }

  removeSms(sms) {
    const availableQuota = this.persistenceService.get(PERSISTANCEKEY.AVAILABLEQUOTA, StorageType.SESSION);
    // availableQuota.sms = availableQuota.sms - sms;
    this.persistenceService.set(PERSISTANCEKEY.AVAILABLEQUOTA, availableQuota, {type: StorageType.SESSION});
    this.balanceEvent.emit(EventEnum.smsUpdated);
  }

  profileUpdated() {
    this.profileEvent.emit(EventEnum.profileUpdated);
  }


}

