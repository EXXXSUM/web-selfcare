import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {PersistenceModule, PersistenceService} from 'angular-persistence';
import {AppComponent} from './app.component';
import {CisComponent} from './cis/cis.component';
import {AppRoutingModule} from './app-routing.module';
import {DataClientService} from './data-client.service';
import {AuthInterceptorService} from './auth-interceptor.service';
import {CisSettingsService} from './cis-settings.service';
import {AuthGuard} from './auth.guard';
import {EventListenerService} from './event-listener.service';
import {ErrorsHandlerService} from './errors-handler.service';
import {CustomPipeModule} from './custom-pipe/custom-pipe.module';
import {AuthService} from './services/auth.service';
import { PreviousRouteService } from './services/previous-route.service';
import { OnboardModule } from './onboard/onboard.module';

@NgModule({
  declarations: [
    AppComponent,
    CisComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule,
    PersistenceModule,
    CustomPipeModule,
    OnboardModule
  ],
  providers: [
    CisSettingsService,
    DataClientService,
    EventListenerService,
    PersistenceService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptorService,
      multi: true
    },
    {
      provide: ErrorHandler,
      useClass: ErrorsHandlerService,
    },
    AuthGuard,
    AuthService,
    PreviousRouteService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
