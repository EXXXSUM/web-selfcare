import {Injectable} from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {PersistenceService, StorageType} from 'angular-persistence';
import {PERSISTANCEKEY} from '../application-constants';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private persistenceService: PersistenceService, private router: Router) {

  }

  canActivate(): boolean {
    const accessToken = this.persistenceService.get(PERSISTANCEKEY.ACCESSTOKEN, StorageType.SESSION);
    if (accessToken) {
      return true;
    } else {
      this.router.navigate(['/public']);
    }
  }
}
