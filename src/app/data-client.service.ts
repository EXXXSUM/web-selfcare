import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';


@Injectable()
export class DataClientService {

  baseUrl: string;
  restUrls: any;
  serverConfig: any;

  constructor(private http: HttpClient) {
    this.baseUrl = this.buildBaseUrl();
  }

  getRestUrls(): any {
    return this.restUrls;
  }

  buildBaseUrl(): string {
    let baseUrl;
    if (this.serverConfig) {
      if (this.serverConfig.sslEnabled) {
        baseUrl = 'https://';
      } else {
        baseUrl = 'http://';
      }
      if (this.serverConfig.domain) {
        baseUrl += this.serverConfig.domain;
        if (this.serverConfig.port) {
          baseUrl += ':' + this.serverConfig.port;
        }
        baseUrl += '/';
      }
      if (this.serverConfig.appContext) {
        baseUrl += this.serverConfig.appContext + '/';
      }
      if (this.serverConfig.restContext) {
        baseUrl += this.serverConfig.restContext + '/';
      }
    } else {
      baseUrl = 'http://125.22.2.38:8180/cisBusiness/rest/';
    }
    return baseUrl;
  }

  buildUrl(url: string, urlParams?: string | string[], searchParams?: {}): string {
    if (urlParams) {
      for (let i = 0; i < urlParams.length; i++) {
        if (urlParams[i].trim() !== '') {
          url += '/' + urlParams[i];
        }
      }
    }
    if (searchParams) {
      url += '?';
      for (const key in searchParams) {
        if (searchParams[key] !== undefined) {
          url += key + '=' + searchParams[key] + '&';
        }
      }
      if (url.lastIndexOf('&') === url.length - 1) {
        url = url.substr(0, url.length - 1);
      }
    }
    if (!url.startsWith('http')) {
      url = this.baseUrl + url;
    }
    return url;
  }

  get(url: string, urlParams?: string | string[], searchParams?: {}, responseType = null): Observable<any> {
    if (responseType === 'text') {
      return this.http.get(this.buildUrl(url, urlParams, searchParams), {
        observe: 'response',
        responseType: 'text'
      });
    } else {
      return this.http.get<any>(this.buildUrl(url, urlParams, searchParams));
    }
  }

  post(data: any, url: string, urlParams?: string | string[], searchParams?: {}, responseType = null): Observable<any> {
    if (responseType === 'text') {
      return this.http.post(this.buildUrl(url, urlParams, searchParams), data, {
        observe: 'response',
        responseType: 'text'
      });
    } else {
      return this.http.post<any>(this.buildUrl(url, urlParams, searchParams), data);
    }
  }

  delete(url: string, urlParams?: string | string[], searchParams?: {}, responseType = null): Observable<any> {
    if (responseType === 'text') {
      return this.http.delete(this.buildUrl(url, urlParams, searchParams), {
        observe: 'response',
        responseType: 'text'
      });
    } else {
      return this.http.delete<any>(this.buildUrl(url, urlParams, searchParams));
    }

  }

  put(data: any, url: string, urlParams?: string | string[], searchParams?: {}, responseType = null): Observable<any> {
    if (responseType === 'text') {
      return this.http.put(this.buildUrl(url, urlParams, searchParams), data, {
        observe: 'response',
        responseType: 'text'
      });
    } else {
      return this.http.put<any>(this.buildUrl(url, urlParams, searchParams), data);
    }

  }
}
