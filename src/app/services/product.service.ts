import {Injectable} from '@angular/core';
import {DataClientService} from '../data-client.service';
import {CustomOffer} from '../interface/custom.offer';
import {CustomizePlanOffer} from '../interface/product';
import * as _ from 'underscore';
import {PLANTYPE, QUOTATYPE} from '../../application-constants';

@Injectable()
export class ProductService {
  constructor(private dataClientService: DataClientService) {
  }

  async getCustomOffer(): Promise<CustomOffer> {
    const offers: CustomizePlanOffer[] = await this.dataClientService.get('product', ['findByQuery'], {
      productType: 'DATA,VOICE,SMS',
      productStatus: 'ACTIVE',
      isRecurring: 'BOTH',
      productGroup: 'CustomizePlanOffer',
    }).toPromise();
    let dataplans = [];
    let voiceplans = [];
    let smsplans = [];
    for (const item of offers) {
      if (item.productType.toLowerCase() === QUOTATYPE.DATA) {
        let text: any = item.quota.data.unit.toString();
        if (parseInt(text, 10) < 0) {
          text = 'unlimited';
        } else {
          text = parseFloat(text) / (1024 * 1024 * 1024);
        }
        dataplans.push({
          name: text,
          price: item.price,
          productId: item.productId
        });
      }
      if (item.productType.toLowerCase() === QUOTATYPE.VOICE) {
        let text: any = item.quota.voice.unit.toString();
        if (parseInt(text, 10) < 0) {
          text = 'unlimited';
        } else {
          text = Number.parseFloat(parseFloat((text / 60).toString()).toFixed(2));
        }
        voiceplans.push({
          name: text,
          price: item.price,
          productId: item.productId
        });
      }
      if (item.productType.toLowerCase() === QUOTATYPE.SMS) {
        let text: any = item.quota.sms.unit.toString();
        if (parseInt(text, 10) < 0) {
          text = 'unlimited';
        } else {
          text = parseFloat(text);
        }
        smsplans.push({
          name: text,
          price: item.price,
          productId: item.productId
        });
      }
    }
    dataplans = _.sortBy(dataplans, (x) => {
      return typeof  x.name;
    });
    dataplans = _.sortBy(dataplans, (x) => {
      return Number.isNaN(x.name) ? x.name : Number.parseFloat(x.name);
    });
    voiceplans = _.sortBy(voiceplans, function (x) {
      return typeof  x.name;
    });
    voiceplans = _.sortBy(voiceplans, function (x) {
      return Number.isNaN(x.name) ? x.name : Number.parseFloat(x.name);
    });
    smsplans = _.sortBy(smsplans, function (x) {
      return typeof x.name;
    });
    smsplans = _.sortBy(smsplans, function (x) {
      return Number.isNaN(x.name) ? x.name : Number.parseFloat(x.name);
    });
    return {
      dataPlans: dataplans,
      voicePlans: voiceplans,
      smsPlans: smsplans,
    };
  }

  async getPlanByType(type): Promise<CustomizePlanOffer> {
    let result = null;
    const customizePlanOffers: CustomizePlanOffer[] = await this.dataClientService.get('product', ['findByQuery'], {
      productType: 'DATA,VOICE,SMS,COMBO',
      productStatus: 'ACTIVE',
      isRecurring: 'BOTH',
      productGroup: type,
    }).toPromise();

    if (customizePlanOffers.length > 0) {
      if (type.toLowerCase().trim() === PLANTYPE.STANDARD) {
        result = customizePlanOffers[0];
      } else if (type.toLowerCase().trim() === PLANTYPE.PREMIUM) {
        result = customizePlanOffers[0];
      }
    }
    return result;
  }

  async getInventory(): Promise<string[]> {
    return await this.dataClientService.get('inventory', null, null, null)
      .toPromise();
  }

  async getUpcomingOffers() {
    return await this.dataClientService.get('product', ['findByQuery'], {
      productType: 'COMBO,DATA,VOICE,SMS',
      productStatus: 'ACTIVE',
      isRecurring: 'BOTH',
      productGroup: 'UpcomingOffer'
    }).toPromise();
  }

  getAddons() {
    return this.dataClientService.get('product', ['findByQuery'], {
      productType: 'DATA,VOICE,SMS,COMBO',
      productStatus: 'ACTIVE',
      isRecurring: 'BOTH',
      productGroup: 'Addon'
    });
  }

  getRechargeOptions(type) {
    return this.dataClientService.get('product', ['findByQuery'], {
      productType: type,
      productStatus: 'ACTIVE',
      isRecurring: 'BOTH',
      productGroup: 'Recharge',
    });
  }

  getProductByPlanId(productId) {
    return this.dataClientService.get('product', [productId]);
  }
}




