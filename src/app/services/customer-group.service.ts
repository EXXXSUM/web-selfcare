import {Injectable} from '@angular/core';
import {DataClientService} from '../data-client.service';

@Injectable({
  providedIn: 'root'
})
export class CustomerGroupService {
  constructor(private dataClientService: DataClientService) {

  }

  async getSharedAccount(msisdn) {
    return await this.dataClientService.get('customerGroup', [msisdn, 'sharedAccount', 'balance']).toPromise();
  }

  removeSharedAccount(mysmsisdn, msisdn) {
    return this.dataClientService.delete('customerGroup', [mysmsisdn, 'sharedAccount', msisdn]);
  }

  getFAF(msisdn) {
    return this.dataClientService.get('customerGroup', ['FAF', msisdn]);
  }

  removeFAF(msisdn, removeId) {
    return this.dataClientService.delete('customerGroup', ['FAFMember', msisdn, removeId]);
  }

  addFAF(msisdn, addmsisdn) {
    return this.dataClientService.post(null, 'customerGroup', ['FAFMember', msisdn, addmsisdn]);
  }

  registerFAF(msisdn) {
    return this.dataClientService.post(null, 'customerGroup', ['FAF', msisdn]);
  }

}
