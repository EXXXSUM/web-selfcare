import {Injectable} from '@angular/core';
import {DataClientService} from '../data-client.service';
import {Subscription} from '../interface/subscription';
import Product = Subscription.Product;

@Injectable({
  providedIn: 'root'
})
export class CustomerService {
  constructor(private dataClientService: DataClientService) {

  }

  registerUser(param) {
    return this.dataClientService.post(param, 'customer', ['register'], null, 'text');
  }

  getCustomerInfo(msisdn) {
    return this.dataClientService.get('customer', [msisdn]);
  }

  getAggregatedSubscription(msisdn) {
    return this.dataClientService.get('customer', ['subscription', 'balance'], {
      msisdn: msisdn,
      subscriberCategory: 'PREPAID'
    });
  }

  getSubscriptionDetail(msisdn) {
    return this.dataClientService.get('customer', ['subscription', 'details'], {msisdn: msisdn});
  }

  removeSharedAccount(mysmsisdn, msisdn) {
    return this.dataClientService.delete('customerGroup', [mysmsisdn, 'sharedAccount', msisdn]);
  }

  removeMsisdnFromInventory(msisdn) {
    return this.dataClientService.delete('inventory', [msisdn]);
  }

  async recharge(msisdn: string, quota: number = 0, type: string = 'credit'): Promise<any> {
    if (quota === 0) {
      throw new Error('Please enter amount');
    }
    return await this.dataClientService.post(null, 'customer', [msisdn, 'balance', type.toString(), quota.toString(10)], null).toPromise();
  }


  async buyPlan(msisdn, productId): Promise<any> {
    const params = {
      msisdn: msisdn,
      productId: productId,
      buyOption: 'RACT'
    };
    return await
      this.dataClientService.post(params, 'customer', ['subscription', 'product'], {}).toPromise();
  }

  async removePlan(msisdn, productId) {
    const params = {
      msisdn: msisdn,
      productId: productId,
      buyOption: 'DACT'
    };
    return await
      this.dataClientService.post(params, 'customer', ['subscription', 'product'], {}).toPromise();
  }

  async getRechargeHistory(msisdn: string, type: string = 'credit') {
    return await this.dataClientService.get('customer', [msisdn, 'balance', type], null).toPromise();
  }

  async getTransferHistory(msisdn) {
    return await this.dataClientService.get('customer', ['subscription', 'transferHistory'], {msisdn: msisdn}).toPromise();
  }


  async subscriptionDetail(msisdn): Promise<{ msisdn: string, products: Product[] }> {
    return await  this.dataClientService.get('customer', ['subscription', 'details'], {msisdn: msisdn}).toPromise();
  }

  private buyProduct(params) {
    return this.dataClientService.post(params, 'customer', ['subscription', 'product'], {});
  }

  shareQuota(params) {
    return this.buyProduct(params);
  }

  transferCreditBalance(param) {
    return this.buyProduct(param);
  }

  addAddon(param) {
    return this.buyProduct(param);
  }

  removeAddon(param) {
    return this.buyProduct(param);
  }

  transferItem(param) {
    return this.buyProduct(param);
  }

  addQuota(param) {
    return this.buyProduct(param);
  }
  setupsisdn(param) {
    return this.buyProduct(param);
  }

}
