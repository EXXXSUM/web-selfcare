import {Injectable} from '@angular/core';
import {DataClientService} from '../data-client.service';
import {PersistenceService, StorageType} from 'angular-persistence';
import {PERSISTANCEKEY} from '../../application-constants';

@Injectable()
export class AuthService {
  constructor(private dataClientService: DataClientService, private  persistanceService: PersistenceService) {

  }

  login(username: string, password: string) {
    return this.dataClientService.get('user', ['login'], {
      username: username,
      password: btoa(password)
    });
  }

  logout() {
    return this.dataClientService.post(null, 'user', ['logout'], null, null);
  }


  getCustomerInfo(msisdn) {
    return this.dataClientService.get('customer', [msisdn]);
  }

  saveProfile(params) {
    return this.dataClientService.put(params, 'customer', [''], null, 'text');
  }

  async refreshToken() {
    const user = JSON.parse(atob(this.persistanceService.get(PERSISTANCEKEY.USER, StorageType.SESSION)));
    const username = user.loginName;
    const password = user.loginName;
    const res = await  this.login(username, password).toPromise();
    const token = res.access_token;
    this.persistanceService.set(PERSISTANCEKEY.ACCESSTOKEN, {type: StorageType.SESSION});
    return token;
  }
}
