import {Injectable} from '@angular/core';
import {Subscription} from '../interface/subscription';
import {PLANTYPE, QUOTATYPE} from '../../application-constants';
import Product = Subscription.Product;

@Injectable({
  providedIn: 'root'
})
export class UtilService {

  constructor() {
  }

  /**
   * Parsing subscription api to get current plan and merge custom plan
   * @param {Product[]} products
   * @returns {Product}
   */
  getCustomerPlanFromSubscription(products: Product[]) {
    let currentPlan: Product = null;
    currentPlan = products.find(x => x.productGroup.toString().toLowerCase() === PLANTYPE.STANDARD || x.productGroup.toString().toLowerCase() === PLANTYPE.PREMIUM);
    if (!currentPlan) {
      /* standard and premium plan not available */
      const customPlans = products.filter(x => x.productGroup.toLowerCase() === PLANTYPE.CUSTOMIZEPLANOFFER);
      if (customPlans.length > 0) {
        /* initiate current plan */
        currentPlan = {
          productId: '',
          productName: null,
          productType: null,
          productGroup: null,
          price: 0,
          paymentMode: null,
          srcChannel: null,
          numberOfPendingRenewals: null,
          nextRenewalDate: null,
          provisionedQuota: {
            data: null,
            voice: null,
            sms: null
          },
          availableQuota: {
            data: null,
            voice: null,
            sms: null,
            other: null
          },
          activationDate: null,
          expiryDate: null,
          isRecurring: null
        };
        customPlans.forEach(x => {
          currentPlan.price = currentPlan.price + x.price;
          currentPlan.productId = currentPlan.productId + (currentPlan.productId === '' ? x.productId : '^^' + x.productId);
          currentPlan.productName = currentPlan.productName + (currentPlan.productName === '' ? x.productName : '^^' + x.productName);
          currentPlan.productType = currentPlan.productType + (currentPlan.productType === '' ? x.productType : '^^' + x.productType);
          currentPlan.productGroup = PLANTYPE.CUSTOM;
          if (x.productType.toString().toLowerCase() === QUOTATYPE.DATA) {
            currentPlan.provisionedQuota.data = x.provisionedQuota.data || null;
          }
          if (x.productType.toString().toLowerCase() === QUOTATYPE.VOICE) {
            currentPlan.provisionedQuota.voice = x.provisionedQuota.voice || null;
          }
          if (x.productType.toString().toLowerCase() === QUOTATYPE.SMS) {
            currentPlan.provisionedQuota.sms = x.provisionedQuota.sms || null;
          }
          if (x.productType.toString().toLowerCase() === QUOTATYPE.DATA) {
            currentPlan.availableQuota.data = x.availableQuota.data || null;
          }
          if (x.productType.toString().toLowerCase() === QUOTATYPE.VOICE) {
            currentPlan.availableQuota.voice = x.availableQuota.voice || null;
          }
          if (x.productType.toString().toLowerCase() === QUOTATYPE.SMS) {
            currentPlan.availableQuota.sms = x.availableQuota.sms || null;
          }
          currentPlan.activationDate = x.activationDate;
          currentPlan.expiryDate = x.expiryDate;
          currentPlan.isRecurring = x.isRecurring;
        });
      }
    }
    return currentPlan;
  }
}
