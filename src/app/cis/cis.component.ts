import {AfterViewInit, Component, OnInit} from '@angular/core';
import {CisSettingsService} from '../cis-settings.service';
import {PersistenceService, StorageType} from 'angular-persistence';
import {Router} from '@angular/router';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {mockUsername} from '../mockData';
import {ProductService} from '../services/product.service';
import {CustomizePlanOffer} from '../interface/product';
import {AuthService} from '../services/auth.service';
import {CustomerService} from '../services/customer.service';
import {PERSISTANCEKEY, ONBOARDPERSISTANCEKEY} from '../../application-constants';

declare const jQuery: any;
declare const alertify;

@Component({
  selector: 'app-cis',
  templateUrl: './cis.component.html',
  styleUrls: ['./cis.component.scss'],
  providers: [ProductService, CustomerService]
})
export class CisComponent implements OnInit, AfterViewInit {

  registerFormModel: FormGroup;
  forgotPFormModel: FormGroup;
  loginForm: FormGroup;
  confirmPasswordModal: FormControl = new FormControl('');
  isMsisdnEntered = false;

  /* Plan description */
  dataPlans = [];
  voicePlans = [];
  smsPlans = [];

  standardPlanOffer: CustomizePlanOffer;
  premiumPlanOffer: CustomizePlanOffer;

  useraddons: CustomizePlanOffer[] = [];
  availableAddons: CustomizePlanOffer[];
  inventory: string[] = [];

  selectedDataPlanIndex = 0;
  selectedVoicePlanIndex = 0;
  selectedSmsPlanIndex = 0;

  quickRechargeForm: FormGroup;

  planIndex = {};


  constructor(private cisss: CisSettingsService,
              private persistenceService: PersistenceService,
              private router: Router,
              private customerService: CustomerService,
              private authService: AuthService,
              private fb: FormBuilder, private planService: ProductService) {

    this.loginForm = fb.group({
      loginName: ['9824222202', [Validators.required, Validators.pattern(new RegExp('[a-zA-Z0-9]'))]],
      password: ['Password', [Validators.required, Validators.minLength(4)]]
    });

    this.registerFormModel = fb.group({
      id: '',
      firstName: ['', [Validators.required, Validators.pattern(new RegExp('[a-zA-Z0-9]'))]],
      lastName: ['', [Validators.required, Validators.pattern(new RegExp('[a-zA-Z0-9]'))]],
      email: ['', [Validators.required, Validators.email, Validators.pattern(new RegExp('[a-zA-Z0-9]'))]],
      password: ['', [Validators.required, Validators.minLength(4)]],
      phoneModel: '',
      deviceOS: '',
      registrationId: '',
      userStatus: '1',
      msisdn: ['', [Validators.required, Validators.pattern(new RegExp('[a-zA-Z0-9]'))]],
      subscriberCategory: 'POSTPAID'
    });
    this.forgotPFormModel = this.fb.group({
      msisdn: ['', Validators.required],
      otp: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(4)]]
    });

    this.quickRechargeForm = new FormGroup({
      msisdn: new FormControl('', [Validators.required, Validators.minLength(10), Validators.maxLength(10)]),
      quota: new FormControl('', [Validators.required, Validators.min(0)])
    });

  }

  ngOnInit() {

    const accessToken = this.persistenceService.get(PERSISTANCEKEY.ACCESSTOKEN, StorageType.SESSION);
    if (accessToken) {
      this.router.navigate(['dashboard']);
    }
    /* for plan info and custom info
     * login using dummy user and use token
     */
    setTimeout(() => {
        this.loginDummyUser();
      }, 1000
    );
  }

  ngAfterViewInit(): void {
    jQuery(document).ready(function () {
      // close collapsible menu when menu item click on responsive mode
      jQuery('#navb .nav-link').click(function () {
        if (window.innerWidth < 992) {
          jQuery('.navbar-toggler').trigger('click');
        }
      });

      // Select all links with hashes
      jQuery('.navbar a[href*="#"]')
      // Remove links that don't actually link to anything
        .not('[href="#"]')
        .not('[href="#0"]')
        .click(function (event) {
          event.preventDefault();
          // On-page links
          // Figure out element to scroll to
          let hasTarget = jQuery(this.hash);
          hasTarget = hasTarget.length ? hasTarget : jQuery('[name=' + this.hash.slice(1) + ']');
          // Does a scroll target exist?
          if (hasTarget.length) {
            // Only prevent default if animation is actually gonna happen
            event.preventDefault();
            jQuery('html, body').animate({
              scrollTop: hasTarget.offset().top
            }, 700, function () {
              // Callback after animation
              // Must change focus!
              const target = jQuery(hasTarget);
              target.focus();
              if (target.is(':focus')) { // Checking if the target was focused
                return false;
              } else {
                target.attr('tabindex', '-1'); // Adding tabindex for elements not focusable
                target.focus(); // Set focus again
              }
            });
          }

        });
    });
  }

  loginDummyUser() {
    this.getDummyToken().then(() => {
      this.getAddons();
      this.getInventory();
      return Promise.all([
        this.planService.getCustomOffer(),
        this.planService.getPlanByType('Premium'),
        this.planService.getPlanByType('Standard')]);
    }).then(data => {
      const [customOffer, premium, standard] = data;
      this.dataPlans = customOffer.dataPlans;
      this.voicePlans = customOffer.voicePlans;
      this.smsPlans = customOffer.smsPlans;
      this.premiumPlanOffer = premium;
      this.standardPlanOffer = standard;
      this.persistenceService.set(ONBOARDPERSISTANCEKEY.CUSTOMOFFER, customOffer, {type: StorageType.SESSION});
      this.persistenceService.set(ONBOARDPERSISTANCEKEY.PREMIUM, premium, {type: StorageType.SESSION});
      this.persistenceService.set(ONBOARDPERSISTANCEKEY.STANDARD, standard, {type: StorageType.SESSION});
      this.removeDummyToken();
    }).catch(error => {
      this.removeDummyToken();
    });
  }

  async getDummyToken() {
    const res = await  this.authService.login('9824000101', 'Password').toPromise();
    if (!this.persistenceService.get(PERSISTANCEKEY.ACCESSTOKEN, StorageType.SESSION)) {
      this.persistenceService.set(PERSISTANCEKEY.ACCESSTOKEN, res.access_token, {type: StorageType.SESSION});
    } else {
      throw  new Error('access-token set');
    }
    return res.access_token;
  }

  removeDummyToken() {
    this.persistenceService.remove(PERSISTANCEKEY.ACCESSTOKEN, StorageType.SESSION);
  }


  doLogin(): void {
    const data = this.loginForm.value;
    this.authService.login(data.loginName, data.password)
      .subscribe(this.onLoginSuccess.bind(this));
  }

  registerUser() {
    if (this.registerFormModel.controls['password'].value !== this.confirmPasswordModal.value) {
      alertify.error('password mismatch');
      return false;
    }
    this.customerService.registerUser(this.registerFormModel.value)
      .subscribe((data) => {
        alertify.success('User Registered Successfully');
        this.registerFormModel.reset();
        this.confirmPasswordModal.reset();
        jQuery('#Register').modal('hide');
      }, error => {
        alertify.error(JSON.parse(error.error).message);
      });
  }

  onLoginSuccess(res: any): void {
    jQuery('#loginForm').modal('hide');
    const data = this.loginForm.value;
    this.persistenceService.set(PERSISTANCEKEY.ACCESSTOKEN, res.access_token, {
      type: StorageType.SESSION
    });
    this.persistenceService.set(PERSISTANCEKEY.MSISDN, data.loginName, {
      type: StorageType.SESSION
    });
    this.getCustomerInfo();
    this.persistenceService.set(PERSISTANCEKEY.USER, btoa(JSON.stringify(data)), {type: StorageType.SESSION});
    setTimeout(() => {
      this.router.navigate(['dashboard']);
    }, 500);
  }

  getCustomerInfo() {
    const msisdn = this.persistenceService.get(PERSISTANCEKEY.MSISDN, StorageType.SESSION);
    this.customerService.getCustomerInfo(msisdn)
      .subscribe(data => {
        this.persistenceService.set(PERSISTANCEKEY.USERNAME, data.firstName + ' ' + data.lastName, {type: StorageType.SESSION});
      }, error => {
        this.persistenceService.set(PERSISTANCEKEY.USERNAME, mockUsername, {type: StorageType.SESSION});
      });
  }

  getAddons() {
    if (!this.availableAddons) {
      this.availableAddons = this.persistenceService.get(ONBOARDPERSISTANCEKEY.USERADDON, StorageType.SESSION);
    }
    this.planService.getAddons()
      .subscribe((data: CustomizePlanOffer[]) => {
        if (Array.isArray(data) && data.length > 0) {
          this.availableAddons = data;
          this.persistenceService.set(ONBOARDPERSISTANCEKEY.USERADDON, this.availableAddons, {type: StorageType.SESSION});
        }
      }, error => {

      });
  }

  getInventory() {
    this.planService.getInventory().then(data => {
      this.inventory = data;
      this.persistenceService.set(ONBOARDPERSISTANCEKEY.INVENTORY, this.inventory, {type: StorageType.SESSION});
    }).catch(error => {
      alertify.error('Error fetching inventory');
    });
  }

  switchToRegister() {
    jQuery('#Register').modal('hide');
    setTimeout(() => {
      jQuery('#loginForm').modal('show');
    }, 300);
  }

  switchToForgot() {
    jQuery('#loginForm').modal('hide');
    setTimeout(() => {
      jQuery('#forgotPasswordForm').modal('show');
    }, 300);
  }

  getSumForCustom() {
    let total = 0;
    if (this.dataPlans.length > 0) {
      total = total + this.dataPlans[this.selectedDataPlanIndex].price;
    }
    if (this.voicePlans.length > 0) {
      total = total + this.voicePlans[this.selectedVoicePlanIndex].price;
    }
    if (this.smsPlans.length > 0) {
      total = total + this.smsPlans[this.selectedSmsPlanIndex].price;
    }
    this.persistenceService.set(ONBOARDPERSISTANCEKEY.CUSTOMPLANPRICE, total, {type: StorageType.SESSION});
    return total;
  }

  selectCustomPlan(type, index) {
    switch (type) {
      case 'sms':
        this.selectedSmsPlanIndex = index;
        break;
      case 'voice':
        this.selectedVoicePlanIndex = index;
        break;
      case 'data':
        this.selectedDataPlanIndex = index;
        break;
    }
    this.planIndex ={
      sms : this.selectedSmsPlanIndex,
      voice : this.selectedVoicePlanIndex,
      data : this.selectedDataPlanIndex
    }
    this.persistenceService.set(ONBOARDPERSISTANCEKEY.CUSTOMPLANINDEX, this.planIndex, {type: StorageType.SESSION});
  }

  quickRecharge() {
    const data = this.quickRechargeForm.value;
    this.getDummyToken().then(() => {
      return this.customerService.recharge(data.msisdn, data.quota * 100);
    }).then(() => {
      alertify.success('Recharge succefull');
      this.quickRechargeForm.reset();
      this.removeDummyToken();
    }).catch(() => {
      this.removeDummyToken();
      alertify.error('Please try again later');
    });
  }

  openRoute(route) {
    this.router.navigate(route.split('/'));
  }

  setOnboardBar(type) {
    this.persistenceService.set(ONBOARDPERSISTANCEKEY.ENABLEREGISTER, false, {type: StorageType.SESSION});
    this.persistenceService.set(ONBOARDPERSISTANCEKEY.ENABLEDELIVERY, false, {type: StorageType.SESSION});
    this.persistenceService.set(ONBOARDPERSISTANCEKEY.ENABLEADDON, false, {type: StorageType.SESSION});
    this.persistenceService.set(ONBOARDPERSISTANCEKEY.ENABLECHECKOUT, false, {type: StorageType.SESSION});
    this.persistenceService.set(ONBOARDPERSISTANCEKEY.CURRENTPLAN, type, {type: StorageType.SESSION});
    this.persistenceService.set(ONBOARDPERSISTANCEKEY.SELECTEDDEVICE, {}, {type: StorageType.SESSION});
    this.persistenceService.set(ONBOARDPERSISTANCEKEY.SELECTEDADDONS, [], {type: StorageType.SESSION});
  }

}





