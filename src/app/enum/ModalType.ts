export enum ModalType {
  data = 'data',
  sms = 'sms',
  voice = 'voice'
}
