export enum EventEnum {
  mainBalanceUpdated,
  dataUpdated,
  voiceUpdated,
  smsUpdated,
  profileUpdated
}
