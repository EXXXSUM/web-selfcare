import {Injectable} from '@angular/core';
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpHeaders, HttpRequest} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import 'rxjs/add/operator/catch';

import {PersistenceService, StorageType} from 'angular-persistence';
import {AuthService} from './services/auth.service';
import {Router} from '@angular/router';
import {PERSISTANCEKEY} from '../application-constants';

declare const alertify;

@Injectable()
export class AuthInterceptorService {

  constructor(private persistenceService: PersistenceService, private authService: AuthService, private router: Router) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const accessToken = this.persistenceService.get(PERSISTANCEKEY.ACCESSTOKEN, StorageType.SESSION);
    let headers: any = {};
    if (accessToken && accessToken !== '') {
      headers = {
        'Content-Type': 'application/json; charset=UTF-8',
        Authorization: 'Bearer ' + accessToken
      };
    } else {
      headers = {
        'Content-Type': 'application/json; charset=UTF-8'
      };
    }
    const clonedRequest = request.clone({
      setHeaders: headers
    });
    return next.handle(clonedRequest)
      .catch((err: any) => {
        if (err instanceof HttpErrorResponse) {
          if (err.status === 401) {
            // alertify.error('session timed out');
            this.persistenceService.removeAll(StorageType.SESSION);
            this.router.navigateByUrl('/public');
          }
        }
        return throwError(err);
      });
  }
}
