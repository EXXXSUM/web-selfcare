import {AfterViewInit, Component, ElementRef, HostListener, OnInit, ViewChild} from '@angular/core';
import {PersistenceService, StorageType} from 'angular-persistence';
import {Router} from '@angular/router';
import {EventListenerService} from '../event-listener.service';
import {DataClientService} from '../data-client.service';
import {mockUsername, NotificationMsg} from '../mockData';
import {EventEnum} from '../enum/EventEnum';
import {AuthService} from '../services/auth.service';
import {CustomerService} from '../services/customer.service';
import {PERSISTANCEKEY} from '../../application-constants';
 declare var $: any;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  providers: [CustomerService]
})
export class DashboardComponent implements OnInit, AfterViewInit {

  constructor(
    private persistenceService: PersistenceService,
    private router: Router,
    private listener: EventListenerService,
    private authservice: AuthService,
    private customerService: CustomerService,
    private dataService: DataClientService) {
    this.msisdn = this.persistenceService.get(PERSISTANCEKEY.MSISDN, StorageType.SESSION);

    this.notificationMsg = NotificationMsg;
    this.getNotificationCount();

  }
  // open and close notificationBox

  msisdn = null;
  notificationMsg = [];
  notificationCount = 0;
  showNotification = false;

  @ViewChild('btnBell') btnBell: ElementRef;

  @HostListener('document:click', ['$event.target']) onDocumentClick(targetElement) {
    if (this.btnBell && this.btnBell.nativeElement.contains(targetElement)) {
      this.showNotification = !this.showNotification;
    } else {
      this.showNotification = false;
    }
  }

  ngOnInit() {
    this.listener.profileEvent.subscribe(data => {
      switch (data) {
        case EventEnum.profileUpdated:
          this.getCustomerInfo();
          break;
      }
    });
  }

  ngAfterViewInit() {
    $('.box-1').click(function (e) {
        if (window.innerWidth <= 991) {
          $('.navbar-toggler').trigger('click');
        }
    });
  }

  logout() {
    this.authservice.logout()
      .subscribe(data => {
      }, error => {
        console.log(error);
      });
    this.persistenceService.removeAll(StorageType.SESSION);
    this.router.navigate(['public']);
  }

  openRoute(route) {
    this.router.navigate(route.split('/'));
  }

  getCustomerInfo() {
    const msisdn = this.persistenceService.get(PERSISTANCEKEY.MSISDN, StorageType.SESSION);
    this.customerService.getCustomerInfo(msisdn)
      .subscribe(data => {
        this.persistenceService.set(PERSISTANCEKEY.USERNAME, data.firstName + ' ' + data.lastName, {type: StorageType.SESSION});
      }, error => {
        this.persistenceService.set(PERSISTANCEKEY.USERNAME, mockUsername, {type: StorageType.SESSION});
      });
  }

  setAsRead(index) {
    this.notificationMsg[index].read = true;
    this.getNotificationCount();
  }

  getNotificationCount() {
    let count = 0;
    this.notificationMsg.forEach(x => {
      if (!x.read) {
        count++;
      }
    });
    this.notificationCount = count;
  }

}
