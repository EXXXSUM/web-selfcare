import {DataClientService} from '../../data-client.service';
import {PERSISTANCEKEY} from '../../../application-constants';
import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {PersistenceService, StorageType} from 'angular-persistence';
import {Router} from '@angular/router';
import {Chart} from 'chart.js';
import {mockDataAvailable, mockDataProvisoned, mockSMSAvailable, mockSMSProvisoned, mockVoiceAvailable, mockVoiceProvisoned} from '../../mockData';
import {EventListenerService} from '../../event-listener.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import * as moment from 'moment';
import {EventEnum} from '../../enum/EventEnum';
import {ProductService} from '../../services/product.service';

import {CustomizePlanOffer} from '../../interface/product';
import {UtilService} from '../../services/util.service';
import {CustomerService} from '../../services/customer.service';
import {Subscription} from '../../interface/subscription';
import Product = Subscription.Product;
import AggregatedUnit = Subscription.AggregatedUnit;

@Component({
  selector: 'app-wallet',
  templateUrl: './wallet.component.html',
  styleUrls: ['./wallet.component.scss'],
  providers: [ProductService, UtilService, CustomerService]
})
export class WalletComponent implements OnInit, AfterViewInit {
  msisdn = '';
  username = '';
  mainBalance: AggregatedUnit;
  balance;

  constructor(private persistenceService: PersistenceService, private customerService: CustomerService, private event: EventListenerService, private dataService: DataClientService) {
  }

  ngOnInit() {
    const mainBalance = this.persistenceService.get(PERSISTANCEKEY.MAINBALANCE, StorageType.SESSION);
    this.msisdn = this.persistenceService.get(PERSISTANCEKEY.MSISDN, StorageType.SESSION);
    this.username = this.persistenceService.get(PERSISTANCEKEY.USERNAME, StorageType.SESSION);
    this.onResult(mainBalance);
    this.event.balanceEvent.subscribe(data => {
      if (data === EventEnum.mainBalanceUpdated) {
        this.getAggregatedSubscription(this.msisdn);
      }
    });
    this.balance = 0;
  }
  onResult(res: { mainBalance: AggregatedUnit }): void {
    if (typeof res.mainBalance.unit !== 'undefined') {
      this.balance = res.mainBalance.unit / 100;
    }
  }
  ngAfterViewInit(): void {
    this.getAggregatedSubscription(this.msisdn);
  }
  getAggregatedSubscription(msisdn): void {
    this.customerService.getAggregatedSubscription(msisdn)
      .subscribe(this.onResult.bind(this)) ;
  }
}
