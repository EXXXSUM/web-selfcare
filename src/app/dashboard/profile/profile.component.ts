import {Component, OnInit} from '@angular/core';
import {DataClientService} from '../../data-client.service';
import {PersistenceService, StorageType} from 'angular-persistence';
import {mockUsername} from '../../mockData';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {EventListenerService} from '../../event-listener.service';
import {AuthService} from '../../services/auth.service';
import {CustomerService} from '../../services/customer.service';
import {PERSISTANCEKEY} from '../../../application-constants';

declare const alertify;

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
  providers: [CustomerService]
})
export class ProfileComponent implements OnInit {

  username = mockUsername;
  msisdn = null;
  profile: User;
  editForm: FormGroup;
  isEdit = false;

  constructor(private db: FormBuilder, private persistenceService: PersistenceService,
              private customerService: CustomerService,
              private dataService: DataClientService, private eventService: EventListenerService,
              private  authService: AuthService) {
    this.editForm = db.group({
      firstName: ['', [Validators.required, Validators.pattern(new RegExp('[a-zA-Z0-9]'))]],
      lastName: ['', [Validators.required, Validators.pattern(new RegExp('[a-zA-Z0-9]'))]],
      email: ['', [Validators.required, Validators.email]],
      id: '',
      password: '',
      phoneModel: '',
      deviceOS: '',
      registrationId: '',
      userStatus: '',
      subscriberCategory: '',
      msisdn: '',
    });
  }

  ngOnInit() {
    this.getCustomerInfo();
  }

  getCustomerInfo() {
    const msisdn = this.persistenceService.get(PERSISTANCEKEY.MSISDN, StorageType.SESSION);
    this.msisdn = msisdn;
    const username = this.persistenceService.get(PERSISTANCEKEY.USERNAME, StorageType.SESSION);
    this.username = username;

    this.customerService.getCustomerInfo(msisdn)
      .subscribe((data: User) => {
        this.profile = data;
        this.setFormData(this.profile);
      }, error => {
        this.profile = {
          'id': 34,
          'firstName': 'SumathiShekar',
          'lastName': 'HR',
          'email': 'test@email.com',
          'password': 'Password',
          'phoneModel': 'iphone8plus',
          'deviceOS': 'iOS',
          'registrationId': '123',
          'userStatus': 1,
          'subscriberCategory': 'A1',
          'msisdn': '876542829'
        };
        this.setFormData(this.profile);
      });
  }

  setFormData(data) {
    this.editForm.patchValue({
      firstName: data.firstName,
      lastName: data.lastName,
      email: data.email,
      id: data.id,
      password: data.password,
      phoneModel: data.phoneModel,
      deviceOS: data.deviceOS,
      registrationId: data.registrationId,
      userStatus: data.userStatus,
      subscriberCategory: data.subscriberCategory,
      msisdn: data.msisdn
    });
  }

  saveProfile() {
    this.authService.saveProfile(this.editForm.value)
      .subscribe((data) => {
        this.isEdit = false;
        alertify.success('Profile Updated Successfully');
        this.eventService.profileUpdated();
        this.username = this.editForm.controls['firstName'].value + ' ' + this.editForm.controls['lastName'].value;
        this.profile = this.editForm.value;
      }, error => {
        console.log(error);
        alertify.error('Something Went Wrong');
      });
  }
}

interface User {
  id: number;
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  phoneModel: string;
  deviceOS: string;
  registrationId: string;
  userStatus: number;
  subscriberCategory: string;
  msisdn: string;
}
