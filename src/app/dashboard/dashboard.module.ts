import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DashboardRoutingModule} from './dashboard-routing.module';
import {DashboardComponent} from './dashboard.component';
import {HomeComponent} from './home/home.component';
import {ProfileComponent} from './profile/profile.component';
import {PlanComponent} from './plan/plan.component';
import {DfyComponent} from './dfy/dfy.component';
import {UtcComponent} from './utc/utc.component';
import {SupportComponent} from './support/support.component';
import {CommonAddModalComponent} from './common-add-modal/common-add-modal.component';
import {CommonShareModalComponent} from './common-share-modal/common-share-modal.component';
import {CommonTransferModalComponent} from './common-transfer-modal/common-transfer-modal.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {WalletComponent} from './wallet/wallet.component';
import {RoamingComponent} from './roaming/roaming.component';
import {CustomPipeModule} from '../custom-pipe/custom-pipe.module';
import {NouisliderModule} from 'ng2-nouislider';
import {CommonUsagetrafficComponent} from './common-usagetraffic/common-usagetraffic.component';
import {ChartsModule} from 'ng2-charts';
import {SharedModuleModule} from '../shared-module/shared-module.module';
@NgModule({
  imports: [
    CommonModule,
    DashboardRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    CustomPipeModule,
    NouisliderModule,
    ChartsModule,
    SharedModuleModule
  ],
  declarations: [
    DashboardComponent,
    HomeComponent,
    ProfileComponent,
    PlanComponent,
    DfyComponent,
    UtcComponent,
    SupportComponent,
    CommonAddModalComponent,
    CommonShareModalComponent,
    CommonTransferModalComponent,
    WalletComponent,
    RoamingComponent,
    CommonUsagetrafficComponent
  ]
})
export class DashboardModule {
}
