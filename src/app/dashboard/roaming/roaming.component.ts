import { Component, OnInit } from '@angular/core';
import {PersistenceService, StorageType} from 'angular-persistence';
import {PERSISTANCEKEY} from '../../../application-constants';

@Component({
  selector: 'app-roaming',
  templateUrl: './roaming.component.html',
  styleUrls: ['./roaming.component.scss']
})
export class RoamingComponent implements OnInit {
  msisdn;
  username;
  constructor(private persistenceService: PersistenceService) {
    this.msisdn = this.persistenceService.get(PERSISTANCEKEY.MSISDN, StorageType.SESSION);
    this.username = this.persistenceService.get(PERSISTANCEKEY.USERNAME, StorageType.SESSION);
  }

  ngOnInit() {
  }

}
