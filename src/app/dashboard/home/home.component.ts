import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {PersistenceService, StorageType} from 'angular-persistence';
import {Router} from '@angular/router';
import {Chart} from 'chart.js';
import {mockDataAvailable, mockDataProvisoned, mockSMSAvailable, mockSMSProvisoned, mockVoiceAvailable, mockVoiceProvisoned} from '../../mockData';
import {EventListenerService} from '../../event-listener.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import * as moment from 'moment';
import {EventEnum} from '../../enum/EventEnum';
import {ProductService} from '../../services/product.service';

import {CustomizePlanOffer} from '../../interface/product';
import {PERSISTANCEKEY, PRODUCTGROUP} from '../../../application-constants';
import {UtilService} from '../../services/util.service';
import {CustomerService} from '../../services/customer.service';
import {Subscription} from '../../interface/subscription';
import Product = Subscription.Product;
import AggregatedUnit = Subscription.AggregatedUnit;

declare const alertify;
declare var jQuery: any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  providers: [ProductService, UtilService, CustomerService]
})
export class HomeComponent implements OnInit, AfterViewInit {

  @ViewChild('chartCanvas1') canvasRef1: ElementRef;
  @ViewChild('chartCanvas2') canvasRef2: ElementRef;
  @ViewChild('chartCanvas3') canvasRef3: ElementRef;
  chart: any;

  addOns: Product[] = [];
  selectedAddOn: any;

  isDataPlanboxSelected: boolean;
  isMinutesPlanboxSelected: boolean;
  isSmsPlanboxSelected: boolean;
  NaN;

  isNext = false;

  mainBalance: AggregatedUnit;
  data: any;
  voice: any;
  sms: any;

  currentAddForm;
  currentTransferForm;
  currentShareForm;

  msisdn;
  username;
  addDataAdons = [];


  provisionedData;
  provisionedVoice;
  provisionedSms;

  activeDataSet = 0;

  creditTransferForm: FormGroup;
  mixCreditBalance = 0;
  maxCreditBalance = 50;
  transfer;
  bar = 0;
  benmsisdn;

  selectedQuota = 'DATA';

  upcommingOffer: CustomizePlanOffer[] = [];

  constructor(private persistenceService: PersistenceService,
              private router: Router, private event: EventListenerService,
              private customerService: CustomerService,
              private fb: FormBuilder,
              private planservice: ProductService,
              private utilService: UtilService) {
    this.creditTransferForm = this.fb.group({
      msisdn: ['', Validators.required],
      benmsisdn: ['', Validators.required],
      transfer: [0, Validators.required],
      srcAccountType: '1',
      dstAccountType: '1',
      buyOption: 'ME2U',
      productId: 'CM_Time2Share_575'
    });
  }

  ngOnInit() {
    this.mainBalance = {'unit': 0 } ;
    this.msisdn = this.persistenceService.get(PERSISTANCEKEY.MSISDN, StorageType.SESSION);
    this.username = this.persistenceService.get(PERSISTANCEKEY.USERNAME, StorageType.SESSION);
    const mainBalance = this.persistenceService.get(PERSISTANCEKEY.MAINBALANCE, StorageType.SESSION);
    if (mainBalance) {
      this.onResult1(mainBalance);
      this.getAggregatedSubscription(this.msisdn);
    } else {
      this.getAggregatedSubscription(this.msisdn);
    }
    this.getSubscriptionDetail();

    const availableQuota = this.persistenceService.get(PERSISTANCEKEY.AVAILABLEQUOTA, StorageType.SESSION);
    const provisionedQuota = this.persistenceService.get(PERSISTANCEKEY.PROVISIONEDQUOTA, StorageType.SESSION);
    const userAddons = this.persistenceService.get(PERSISTANCEKEY.USERADDON, StorageType.SESSION);

    if (userAddons) {
      this.addOns = userAddons;
    }
    this.initChart();

    this.event.balanceEvent.subscribe(data => {
      if (data === EventEnum.mainBalanceUpdated) {
        this.getAggregatedSubscription(this.msisdn);
      }
      if (data === EventEnum.dataUpdated || data === EventEnum.voiceUpdated || data === EventEnum.smsUpdated) {
        this.getSubscriptionDetail();
      }
    });
    this.getUpcomingOffers();
  }

  ngAfterViewInit(): void {
    const availableQuota = this.persistenceService.get(PERSISTANCEKEY.AVAILABLEQUOTA, StorageType.SESSION);
    const provisionedQuota = this.persistenceService.get(PERSISTANCEKEY.PROVISIONEDQUOTA, StorageType.SESSION);
    jQuery('#creditTranser').on('hidden.bs.modal', (e) => {
      this.benmsisdn = '';
      this.bar = 0;
      this.transfer = '';
    });
  }

  getAggregatedSubscription(msisdn): void {
    this.customerService.getAggregatedSubscription(msisdn)
      .subscribe(this.onResult.bind(this), this.onError.bind(this));
  }

  onResult(res: { mainBalance: AggregatedUnit }): void {
    const mainBalance = this.persistenceService.get(PERSISTANCEKEY.MAINBALANCE, StorageType.SESSION);
    if (!mainBalance) {
      this.persistenceService.set(PERSISTANCEKEY.MAINBALANCE, res, {type: StorageType.SESSION});
    }
    this.mainBalance = res.mainBalance;
    if (typeof this.mainBalance.unit !== 'undefined') {
      this.maxCreditBalance = this.mainBalance.unit / 100;
      if (this.maxCreditBalance === 0) {
        this.maxCreditBalance = 0.1;
      }
    }
    this.bar = 0;
    this.transfer = '';
  }

  onResult1(res: { mainBalance: AggregatedUnit }): void {
    const mainBalance = this.persistenceService.get(PERSISTANCEKEY.MAINBALANCE, StorageType.SESSION);
    if (!mainBalance) {
      this.persistenceService.set(PERSISTANCEKEY.MAINBALANCE, res, {type: StorageType.SESSION});
    }
    this.mainBalance = res.mainBalance;
    if (typeof this.mainBalance.unit !== 'undefined') {
      this.maxCreditBalance = this.mainBalance.unit / 100;
      if (this.maxCreditBalance === 0) {
        this.maxCreditBalance = 0.1;
      }
    }
    this.bar = 0;
    this.transfer = '';
    this.mainBalance = {'unit': 0 } ;
  }

  onError(res: any): void {
    this.onResult({
      mainBalance: {unit: 5000, expiryDate: new Date().toDateString()}
    });
  }

  getSubscriptionDetail() {
    const msisdn = this.persistenceService.get(PERSISTANCEKEY.MSISDN, StorageType.SESSION);
    this.customerService.getSubscriptionDetail(msisdn)
      .subscribe((data: { msisdn: string, products: Product[] }) => {
        let currentPlan: Product = null;
        if (data.products) {
          currentPlan = this.utilService.getCustomerPlanFromSubscription(data.products);
          if (currentPlan) {
            if (currentPlan.availableQuota) {
              let dataUnit, voiceUnit, smsUnit;
              if ((currentPlan.availableQuota.data && currentPlan.availableQuota.data.unit === 0) || currentPlan.availableQuota.data == null) {
                dataUnit = 0;
              } else {
                dataUnit = currentPlan.availableQuota.data && currentPlan.availableQuota.data.unit ? currentPlan.availableQuota.data.unit : -1;
              }
              if ((currentPlan.availableQuota.voice && currentPlan.availableQuota.voice.unit === 0) || currentPlan.availableQuota.voice == null) {
                voiceUnit = 0;
              } else {
                voiceUnit = currentPlan.availableQuota.voice && currentPlan.availableQuota.voice.unit ? currentPlan.availableQuota.voice.unit : -1;
              }
              if ((currentPlan.availableQuota.sms && currentPlan.availableQuota.sms.unit === 0) || currentPlan.availableQuota.sms == null) {
                smsUnit = 0;
              } else {
                smsUnit = currentPlan.availableQuota.sms && currentPlan.availableQuota.sms.unit ? currentPlan.availableQuota.sms.unit : -1;
              }
              this.setAvailableQuota(dataUnit, voiceUnit, smsUnit);
            } else {
              this.setAvailableQuota(-1, -1, -1);
            }
            if (currentPlan.provisionedQuota) {
              this.setProvisionedQuota(currentPlan.provisionedQuota.data && currentPlan.provisionedQuota.data.unit ? currentPlan.provisionedQuota.data.unit : 0, currentPlan.provisionedQuota.voice && currentPlan.provisionedQuota.voice.unit ? currentPlan.provisionedQuota.voice.unit : 0, currentPlan.provisionedQuota.sms && currentPlan.provisionedQuota.sms.unit ? currentPlan.provisionedQuota.sms.unit : 0);
            } else {
              this.setProvisionedQuota(-1, -1, -1);
            }
          } else {
            this.setAvailableQuota(mockDataAvailable, mockVoiceAvailable, mockSMSAvailable);
            this.setProvisionedQuota(mockDataProvisoned, mockVoiceProvisoned, mockSMSProvisoned);
          }
          this.addOns = data.products.filter((x: Product) => {
            let expDateString: string = x.expiryDate.split(' ')[0];
            expDateString = expDateString.split('-').reverse().join('-');
            const expDate = new Date(expDateString);
            const currentDate = new Date(new Date().setHours(0, 0, 0, 0));
            return x.productGroup.toString().toLowerCase() === PRODUCTGROUP.ADDON && expDate.getTime() > currentDate.getTime();
          });
          this.persistenceService.set(PERSISTANCEKEY.CURRENTPLAN, currentPlan, {type: StorageType.SESSION});
          this.persistenceService.set(PERSISTANCEKEY.USERADDON, this.addOns, {type: StorageType.SESSION});
        }
      }, error => {
        this.setAvailableQuota(mockDataAvailable, mockVoiceAvailable, mockSMSAvailable);
        this.setProvisionedQuota(mockDataProvisoned, mockVoiceProvisoned, mockSMSProvisoned);
      });
  }

  setProvisionedQuota(data, voice, sms) {
    this.provisionedData = data;
    this.provisionedVoice = voice;
    this.provisionedSms = sms;

    const provisionedQuota = this.persistenceService.get(PERSISTANCEKEY.PROVISIONEDQUOTA, StorageType.SESSION);
    if (!provisionedQuota) {
      this.persistenceService.set(PERSISTANCEKEY.PROVISIONEDQUOTA, {
        data: data,
        voice: voice,
        sms: sms
      }, {type: StorageType.SESSION});
    }
  }

  setAvailableQuota(data, voice, sms) {
    this.data = data;
    this.voice = voice;
    this.sms = sms;
    this.event.customerDataChanges(data, voice, sms);

    const availableQuota = this.persistenceService.get(PERSISTANCEKEY.PROVISIONEDQUOTA, StorageType.SESSION);
    if (!availableQuota) {
      this.persistenceService.set(PERSISTANCEKEY.AVAILABLEQUOTA, {
        data: data,
        voice: voice,
        sms: sms
      }, {type: StorageType.SESSION});
    }
  }

  makePlanboxActive(planboxType: string): void {
    this.selectedQuota = planboxType;
    if (planboxType === 'DATA') {
      this.isDataPlanboxSelected = true;
      this.isMinutesPlanboxSelected = false;
      this.isSmsPlanboxSelected = false;
    } else if (planboxType === 'MINUTES') {
      this.isDataPlanboxSelected = false;
      this.isMinutesPlanboxSelected = true;
      this.isSmsPlanboxSelected = false;
    } else if (planboxType === 'SMS') {
      this.isDataPlanboxSelected = false;
      this.isMinutesPlanboxSelected = false;
      this.isSmsPlanboxSelected = true;
    }
  }


  selectAddOn(addOne: any): void {
    addOne.isSelected = true;
    this.selectedAddOn.isSelected = false;
    this.selectedAddOn = addOne;
  }

  initChart(): void {
    const labels: string[] = [];
    let currentDate = new Date();
    for (let i = 0; i < 6; i++) {
      const label = moment(currentDate).format('DD-MM-YY');
      labels.push(label);
      currentDate = new Date(new Date().setDate(currentDate.getDate() - 1));
    }
    labels.reverse();

    const dataSet1 = this.generateRandomArray(6, 100);
    const dataSet2 = this.generateRandomArray(6, 100);
    const dataSet3 = this.generateRandomArray(6, 1000);

    this.createGraphSet(this.canvasRef1.nativeElement, labels, dataSet1, 'rgba(32, 153, 239, 0.2)', 'Data(GB)');
    this.createGraphSet(this.canvasRef2.nativeElement, labels, dataSet2, 'rgba(244, 191, 4, 0.2)', 'Voice(MIN)');
    this.createGraphSet(this.canvasRef3.nativeElement, labels, dataSet3, 'rgba(225, 17, 0, 0.2)', 'SMS');
  }

  createGraphSet(ref, labels, dataset, rgba, label = '') {
    const colorArray = [];
    for (let i = 0; i < labels.length; i++) {
      colorArray.push(rgba);
    }
    this.chart = new Chart(ref, {
      type: 'line',
      data: {
        labels: labels,
        datasets: [{
          label: label,
          data: dataset,
          backgroundColor: colorArray,
          borderColor: colorArray,
          borderWidth: 1
        }]
      },
      options: {
        scales: {
          yAxes: [{
            ticks: {
              display: true
            }
          }]
        },
        legend: {
          display: true
        }
      }
    });
  }

  setActive(val) {
    this.activeDataSet = val;
  }

  generateRandomArray(limit, limiter = 100): number[] {
    const data = [];
    for (let i = 0; i < limit; i++) {
      const rand = Math.floor(Math.random() * limiter);
      data.push(rand);
    }
    return data;
  }


  addCreditBalance() {
    this.customerService.recharge(this.msisdn, this.activeDataSet * 100).then(data => {
      alertify.success('Credit added successfully');
      this.event.addToMainBalance(this.activeDataSet * 100);
      this.activeDataSet = 0;
    }).catch(error => {
      alertify.error('Error adding creit balance');
    });
  }

  transferCreditBalance() {
    jQuery('#creditTranser').modal('hide');
    const params: any = this.creditTransferForm.value;
    params.benmsisdn = this.benmsisdn.toString();
    params.msisdn = this.msisdn.toString();
    params.transfer = (this.transfer * 100).toString(10);
    this.customerService.transferCreditBalance(params)
      .subscribe((data) => {
        alertify.success('Transferred Successfully');
        this.event.removeFromMainBalance(this.transfer * 100);
      }, error => {
        this.event.removeFromMainBalance(this.transfer * 100);
      }, () => {

      });
  }

  confirmRemoveAddon(index) {
    const name = this.addOns[index].productName;
    const productId = this.addOns[index].productId;
    this.addOns.splice(index, 1);
    this.persistenceService.set(PERSISTANCEKEY.USERADDON, this.addOns, {type: StorageType.SESSION});
    alertify.success('Removed ' + name);
    const params: any = {};
    params.msisdn = this.msisdn;
    params.productId = productId;
    params.buyOption = 'DACT';

    this.customerService.removeAddon(params)
      .subscribe((data) => {
      }, error => {
      }, () => {

      });
  }

  onChange(event) {
    if (typeof event !== 'undefined' && event) {
      this.transfer = event;
      this.bar = event;
    }
  }

  getUpcomingOffers() {
    this.planservice.getUpcomingOffers()
      .then((data: CustomizePlanOffer[]) => {
        this.upcommingOffer = data;
      }).catch((error) => {
      console.log(error);
    });
  }

}
