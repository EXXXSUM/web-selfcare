import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {PersistenceService, StorageType} from 'angular-persistence';
import * as GoogleMapsLoader from 'google-maps';
import {FAQ} from '../../mockData';
import {PERSISTANCEKEY} from '../../../application-constants';

declare const $;

@Component({
  selector: 'app-support',
  templateUrl: './support.component.html',
  styleUrls: ['./support.component.scss']
})
export class SupportComponent implements OnInit, AfterViewInit {
  activeTab = 'FAQ';
  msisdn;
  username;
  locateDisplay = true;
  searchDisplay = true;
  faq = FAQ;


  constructor(private persistenceService: PersistenceService) {
    this.msisdn = this.persistenceService.get(PERSISTANCEKEY.MSISDN, StorageType.SESSION);
    this.username = this.persistenceService.get(PERSISTANCEKEY.USERNAME, StorageType.SESSION);
    GoogleMapsLoader.KEY = 'AIzaSyAh5UmEVGCfJriJqSFFsM7xZ4KbbN_W12E';
  }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
    $('.collapse').collapse();
  }

  loadGoogleMap() {
    GoogleMapsLoader.load(this.onGoogleMapLoad);
  }

  onGoogleMapLoad(google) {
    const uluru = {lat: -25.344, lng: 129.036};
    const marker1 = {lat: -24.00, lng: 131.036};
    const marker2 = {lat: -25.000, lng: 129.036};
    const marker3 = {lat: -26.000, lng: 131.036};
    const map = new google.maps.Map($('#gmap')[0], {zoom: 8, center: uluru});
    const markerPlot1 = new google.maps.Marker({position: marker1, map: map});
    const markerPlot2 = new google.maps.Marker({position: marker2, map: map});
    const markerPlot3 = new google.maps.Marker({position: marker3, map: map});
  }

  selectTab(tab) {
    this.activeTab = tab;
    this.locateDisplay = true;
    if (tab === 'Find a Store' || tab === 'Network Coverage') {
      setTimeout(() => {
        this.loadGoogleMap();
      }, 500);
    } else if (tab === 'FAQ') {
      setTimeout(() => {
        $('.collapse').collapse();
      }, 50);
    }
  }

  locateMinimize() {
    this.locateDisplay = !this.locateDisplay;
    if (!this.locateDisplay) {
      $('.ent-cap-2').animate({marginTop: '-100px'}, 400);
    } else {
      $('.ent-cap-2').animate({marginTop: '-400px'}, 400);
    }
  }

  searchMinimize() {
    this.searchDisplay = !this.searchDisplay;
    if (!this.searchDisplay) {
      $('.ent-cap').animate({marginTop: '-100px'}, 400);
    } else {
      $('.ent-cap').animate({marginTop: '-300px'}, 400);
    }
  }

}


