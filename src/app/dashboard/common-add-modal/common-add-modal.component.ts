import {AfterViewInit, Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {mockaddDataAddOnSet, mockDataSMSAddOn, mockDataVoiceAddOn} from '../../mockData';
import {EventListenerService} from '../../event-listener.service';
import {CustomizePlanOffer} from '../../interface/product';
import {ProductService} from '../../services/product.service';
import {PersistenceService, StorageType} from 'angular-persistence';
import {CustomerService} from '../../services/customer.service';
import {PERSISTANCEKEY} from '../../../application-constants';
import {ModalType} from '../../enum/ModalType';

declare const alertify;
declare var jQuery: any;

@Component({
  selector: 'app-common-add-modal',
  templateUrl: './common-add-modal.component.html',
  styleUrls: ['./common-add-modal.component.scss'],
  providers: [ProductService, CustomerService]
})
export class CommonAddModalComponent implements OnInit, OnChanges, AfterViewInit {
  @Input() type;
  addOns: CustomizePlanOffer[] = [];
  title;
  mockDataSet = mockaddDataAddOnSet;
  mockDataVoiceSet = mockDataVoiceAddOn;
  mockDataSmsSet = mockDataSMSAddOn;
  addOnsDataSets: CustomizePlanOffer[] = [];
  activeDataSet = 0;
  activeDataSetData = 0;
  msisdn;

  isNext = false;

  constructor(private event: EventListenerService,
              private planService: ProductService,
              private customerService: CustomerService,
              private persistenceService: PersistenceService) {
    this.msisdn = this.persistenceService.get(PERSISTANCEKEY.MSISDN, StorageType.SESSION);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.type === ModalType.data) {
      this.title = 'Add Data';
    }
    if (this.type === ModalType.sms) {
      this.title = 'Add SMS';
    }
    if (this.type === ModalType.voice) {
      this.title = 'Add Minutes';
    }
    this.activeDataSet = 0;
    this.isNext = false;
    this.addOnsDataSets = [];
    if (typeof this.type !== 'undefined') {
      this.getDataAddOns(this.type.toUpperCase());
    }
  }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
    jQuery('#addData').on('hidden.bs.modal', (e) => {
      this.isNext = false;
      this.addOnsDataSets.forEach(x => x.isSelected = false);
    });
  }


  getDataAddOns(type) {
    this.planService.getRechargeOptions(type)
      .subscribe((data: CustomizePlanOffer[]) => {
        this.addOns = data;
        for (const items of this.addOns) {
          this.addOnsDataSets.push(items);
        }
        if (this.addOnsDataSets.length === 0) {
          if (this.type === ModalType.data) {
            this.addOnsDataSets = this.mockDataSet;
          }
          if (this.type === ModalType.sms) {
            this.addOnsDataSets = this.mockDataSmsSet;
          }
          if (this.type === ModalType.voice) {
            this.addOnsDataSets = this.mockDataVoiceSet;
          }
        }
      }, error => {
        console.log(error, 'err');
        // alertify.error(error.error);
      });
  }

  setActive(i) {
    this.addOnsDataSets.forEach((res) => {
      res.isSelected = false;
    });
    this.addOnsDataSets[i].isSelected = !this.addOnsDataSets[i].isSelected;
    this.activeDataSet = this.addOnsDataSets[i].price;
    if (this.type === ModalType.data) {
      this.activeDataSetData = this.addOnsDataSets[i].quota.data.unit;
    }
    if (this.type === ModalType.voice) {
      this.activeDataSetData = this.addOnsDataSets[i].quota.voice.unit;
    }
    if (this.type === ModalType.sms) {
      this.activeDataSetData = this.addOnsDataSets[i].quota.sms.unit;
    }
  }

  addData() {
    const productId = this.addOnsDataSets.find(x => x.isSelected).productId;
    const param = {
      'msisdn': this.msisdn,
      'productId': productId,
      'buyOption': 'NACT'
    };
    this.customerService.addQuota(param).subscribe(data => {
      alertify.success(this.type + ' added successfully');
      if (this.type === ModalType.data) {
        this.event.addData(this.activeDataSetData, this.activeDataSet);
      }
      if (this.type === ModalType.voice) {
        this.event.addMinutes(this.activeDataSetData, this.activeDataSet);
      }
      if (this.type === ModalType.sms) {
        this.event.addSMS(this.activeDataSetData, this.activeDataSet);
      }
      jQuery('#addData').modal('hide');
    }, error => {
      alertify.error(`error adding ${this.type}`);

    });
  }
}
