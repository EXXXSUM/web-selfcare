import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommonAddModalComponent } from './common-add-modal.component';

describe('CommonAddModalComponent', () => {
  let component: CommonAddModalComponent;
  let fixture: ComponentFixture<CommonAddModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommonAddModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommonAddModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
