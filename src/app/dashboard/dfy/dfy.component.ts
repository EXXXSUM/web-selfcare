import { Component, OnInit } from '@angular/core';
import { PersistenceService, StorageType } from 'angular-persistence';
import { PERSISTANCEKEY } from '../../../application-constants';
import { ProductService } from '../../services/product.service';
import { CustomizePlanOffer } from '../../interface/product';
import { CustomerService } from '../../services/customer.service';

declare const alertify;
declare var jQuery: any;

@Component({
  selector: 'app-dfy',
  templateUrl: './dfy.component.html',
  styleUrls: ['./dfy.component.scss'],
  providers: [ProductService]
})
export class DfyComponent implements OnInit {
  msisdn;
  username;
  products: CustomizePlanOffer[] = [];
  title = 'Buy Deal';
  productId ;

  constructor(private persistenceService: PersistenceService, private productService: ProductService,
    private customerService: CustomerService) {
    this.msisdn = this.persistenceService.get(PERSISTANCEKEY.MSISDN, StorageType.SESSION);
    this.username = this.persistenceService.get(PERSISTANCEKEY.USERNAME, StorageType.SESSION);
  }

  ngOnInit() {
    this.getUpcomingOffers();
  }

  getUpcomingOffers() {
    this.productService.getUpcomingOffers().then(data => {
      this.products = data;
    }).catch(error => {
      alertify.error('Error fetching deals');
    });
  }

  buyDeal(productId) {
    const param = {
      'msisdn': this.msisdn,
      'productId': productId,
      'buyOption': 'NACT'
    };
    this.customerService.addQuota(param).subscribe(data => {
      alertify.success('Deal bought successfully');
      jQuery('#buyDeal').modal('hide');
    }, error => {
      alertify.error(`error buying deals`);

    });
  }

  cancelDeal() {
    jQuery('#buyDeal').modal('hide');
  }

}
