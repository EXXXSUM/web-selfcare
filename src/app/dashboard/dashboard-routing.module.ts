import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DashboardComponent} from './dashboard.component';


import {PlanComponent} from './plan/plan.component';
import {DfyComponent} from './dfy/dfy.component';
import {UtcComponent} from './utc/utc.component';
import {SupportComponent} from './support/support.component';
import {ProfileComponent} from './profile/profile.component';
import {HomeComponent} from './home/home.component';
import {WalletComponent} from './wallet/wallet.component';
import {RoamingComponent} from './roaming/roaming.component';

const routes: Routes = [{
  path: '',
  component: DashboardComponent,
  children: [{
    path: 'home', component: HomeComponent
  }, {
    path: 'profile', component: ProfileComponent
  }, {
    path: 'plan', component: PlanComponent
  }, {
    path: 'dfy', component: DfyComponent
  }, {
    path: 'utc', component: UtcComponent
  }, {
    path: 'support', component: SupportComponent
  }, {
    path: 'wallet', component: WalletComponent
  }, {
    path: 'roaming', component: RoamingComponent
  }, {
    path: '', redirectTo: 'home', pathMatch: 'full'
  }]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule {
}
