import {Component, OnInit} from '@angular/core';
import {PersistenceService, StorageType} from 'angular-persistence';
import {PERSISTANCEKEY} from '../../../application-constants';

@Component({
  selector: 'app-utc',
  templateUrl: './utc.component.html',
  styleUrls: ['./utc.component.scss']
})
export class UtcComponent implements OnInit {
  activeTab = 0;
  msisdn;
  username;
  constructor(private persistenceService: PersistenceService) {
    this.msisdn = this.persistenceService.get(PERSISTANCEKEY.MSISDN, StorageType.SESSION);
    this.username = this.persistenceService.get(PERSISTANCEKEY.USERNAME, StorageType.SESSION);
  }

  ngOnInit() {
  }



}
