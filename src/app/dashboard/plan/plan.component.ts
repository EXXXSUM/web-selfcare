import {Component, OnInit} from '@angular/core';
import {PersistenceService, StorageType} from 'angular-persistence';
import {FormControl, FormGroup, Validators} from '@angular/forms';

import {ProductService} from '../../services/product.service';
import {CustomizePlanOffer} from '../../interface/product';

import {PERSISTANCEKEY, PLANTYPE} from '../../../application-constants';
import {UtilService} from '../../services/util.service';
import {CustomerService} from '../../services/customer.service';
import {CustomerGroupService} from '../../services/customer-group.service';
import {Subscription} from '../../interface/subscription';
import Product = Subscription.Product;

declare const alertify;
declare const $;

@Component({
  selector: 'app-plan',
  templateUrl: './plan.component.html',
  styleUrls: ['./plan.component.scss'],
  providers: [ProductService, UtilService, CustomerService, CustomerGroupService]
})
export class PlanComponent implements OnInit {

  FAFmsisdns: number[] = [];
  formGroup: FormGroup = new FormGroup({
    msisdn: new FormControl('', [Validators.required, Validators.min(0)])
  });
  showPayment = false;
  showPaymentPremium = false;
  showPaymentCustom = false;
  switchPlan = false;
  hideBuyOption = false;

  dataPlans = [];
  voicePlans = [];
  smsPlans = [];

  selectedDataPlanIndex = -1;
  selectedVoicePlanIndex = -1;
  selectedSmsPlanIndex = -1;

  availableAddons: CustomizePlanOffer[];
  msisdn;
  username;
  useraddons: CustomizePlanOffer[] = [];
  currentPlan: Product;
  currentPlanDetail: CustomizePlanOffer;
  planFeatures1: string[] = [];
  planFeatures2: string[] = [];

  standardPlan: CustomizePlanOffer;
  premiumPlan: CustomizePlanOffer;
  inventory: string[] = [];
  suspendSim;
  rechargeForm: FormGroup;

  constructor(
    private persistenceService: PersistenceService,
    private utilService: UtilService,
    private customerService: CustomerService,
    private customerGroupService: CustomerGroupService,
    private planService: ProductService) {
    this.msisdn = this.persistenceService.get(PERSISTANCEKEY.MSISDN, StorageType.SESSION);
    this.username = this.persistenceService.get(PERSISTANCEKEY.USERNAME, StorageType.SESSION);

    this.rechargeForm = new FormGroup({
      msisdn: new FormControl('', Validators.required),
      quota: new FormControl('', Validators.required)
    });
  }

  ngOnInit() {
    this.getFAF();
    this.getCustomOffer();
    this.getAddons();
    this.getCurrentPlan();
    this.getPlanByType('Premium');
    this.getPlanByType('Standard');
    this.getSubscriptionDetail();
    this.getInventory();
  }

  getCurrentPlan() {
    this.currentPlan = this.persistenceService.get(PERSISTANCEKEY.CURRENTPLAN, StorageType.SESSION);
    console.log(this.currentPlan);
    if (this.currentPlan && this.currentPlan.productGroup.toLowerCase() !== PLANTYPE.CUSTOM) {
      this.planFeatures1 = [];
      this.planFeatures2 = [];
      this.getProductByPlanId(this.currentPlan.productId);
    }
  }

  getProductByPlanId(productId) {
    this.planService.getProductByPlanId(productId)
      .subscribe((data: CustomizePlanOffer) => {
        this.currentPlanDetail = data;
        const features = data.productDescription.replace('\r\n', '').split('|').map(x => x.trim());
        const mid = Math.floor((features.length + 1) / 2);
        this.planFeatures1 = features.slice(0, mid);
        this.planFeatures2 = features.slice(mid);
      }, error => {
        console.log(error);
      });
  }

  getPlanByType(type) {
    this.planService.getPlanByType(type).then(data => {
      if (type.toLowerCase().trim() === PLANTYPE.STANDARD) {
        this.standardPlan = data;
        console.log(this.standardPlan);
      } else if (type.toLowerCase().trim() === PLANTYPE.PREMIUM) {
        this.premiumPlan = data;
      }
    }).catch(error => {
      console.log('Error fetching plan ' + error);
    });
  }


  showPlans() {
    if (!this.switchPlan) {
      this.switchPlan = true;
      this.showPayment = false;
      this.hideBuyOption = false;
    }
  }

  getFAF() {
    const msisdn = this.persistenceService.get(PERSISTANCEKEY.MSISDN, StorageType.SESSION);
    this.customerGroupService.getFAF(msisdn)
      .subscribe((data) => {
        this.FAFmsisdns = data;
        if (this.FAFmsisdns.length > 5) {
          this.FAFmsisdns = this.FAFmsisdns.slice(0, 5);
        }
      }, error => {

      });
  }

  removeFAF(removeId) {
    const msisdn = this.persistenceService.get(PERSISTANCEKEY.MSISDN, StorageType.SESSION);
    this.customerGroupService.removeFAF(msisdn, removeId)
      .subscribe((data) => {
        this.getFAF();
        alertify.success(`${removeId} has been removed`);
      }, error => {
        alertify.error('Error deleting FAF');
      });
  }

  addFAF() {
    if (this.FAFmsisdns.length >= 5) {
      return alertify.error('Cannot add more than 5 FAF');
    }
    const msisdn = this.persistenceService.get(PERSISTANCEKEY.MSISDN, StorageType.SESSION);
    const addmsisdn = this.formGroup.controls[PERSISTANCEKEY.MSISDN].value.toString().trim();

    this.customerGroupService.addFAF(msisdn, addmsisdn)
      .subscribe((data) => {
        alertify.success('Added FAF');
        this.formGroup.patchValue({
          msisdn: '',
        });
        this.getFAF();
      }, error => {
        alertify.error('Error adding FAF');
      });
  }

  registerFAF() {
    const msisdn = this.persistenceService.get(PERSISTANCEKEY.MSISDN, StorageType.SESSION);
    this.customerGroupService.registerFAF(msisdn)
      .subscribe((data) => {
      }, error => {
        alertify.error('Error adding FAF');
      });
  }

  getAddons() {
    let useraddon: CustomizePlanOffer[] = this.persistenceService.get(PERSISTANCEKEY.USERADDON, StorageType.SESSION);
    if (!Array.isArray(useraddon)) {
      useraddon = [];
    }
    this.useraddons = useraddon;
    this.planService.getAddons()
      .subscribe((data: CustomizePlanOffer[]) => {
        if (Array.isArray(data) && data.length > 0) {
          this.availableAddons = data.filter(x => {
            return this.useraddons.findIndex(u => x.productId === u.productId) < 0;
          });
        }
      }, error => {

      });
  }

  getCustomOffer() {
    this.planService
      .getCustomOffer()
      .then(data => {
        this.dataPlans = data.dataPlans;
        this.smsPlans = data.smsPlans;
        this.voicePlans = data.voicePlans;
      }).catch(error => {
      alertify.error('Error fetching custom offer');
    });
  }

  selectCustomPlan(type, index) {
    switch (type) {
      case 'sms':
        if (this.selectedSmsPlanIndex !== index) {
          this.selectedSmsPlanIndex = index;
        } else {
          this.selectedSmsPlanIndex = -1;
        }
        break;
      case 'voice':
        if (this.selectedVoicePlanIndex !== index) {
          this.selectedVoicePlanIndex = index;
        } else {
          this.selectedVoicePlanIndex = -1;
        }
        break;
      case 'data':
        if (this.selectedDataPlanIndex !== index) {
          this.selectedDataPlanIndex = index;
        } else {
          this.selectedDataPlanIndex = -1;
        }
        break;
    }
  }

  getSumForCustom() {
    let total = 0;
    if (this.dataPlans.length > 0 && this.selectedDataPlanIndex > -1) {
      total = total + this.dataPlans[this.selectedDataPlanIndex].price;
    }
    if (this.voicePlans.length > 0 && this.selectedVoicePlanIndex > -1) {
      total = total + this.voicePlans[this.selectedVoicePlanIndex].price;
    }
    if (this.smsPlans.length > 0 && this.selectedSmsPlanIndex > -1) {
      total = total + this.smsPlans[this.selectedSmsPlanIndex].price;
    }
    return total;
  }

  confirmRemoveAddon(index) {
    const name = this.useraddons[index].productName;
    const productId = this.useraddons[index].productId;
    this.availableAddons.push(this.useraddons[index]);
    this.useraddons.splice(index, 1);
    alertify.success('Removed ' + name);
    this.persistenceService.set(PERSISTANCEKEY.USERADDON, this.useraddons, {type: StorageType.SESSION});
    const params: any = {};
    params.msisdn = this.msisdn;
    params.productId = productId;
    params.buyOption = 'DACT';
    this.customerService.removeAddon(params)
      .subscribe((data) => {
      }, error => {
      }, () => {

      });
  }

  confirmAddAddon(index) {
    const name = this.availableAddons[index].productName;
    const productId = this.availableAddons[index].productId;
    const params: any = {};
    params.msisdn = this.msisdn;
    params.productId = productId;
    params.buyOption = 'NACT';
    this.customerService.addAddon(params)
      .subscribe((data) => {
        this.useraddons.push(this.availableAddons[index]);
        this.availableAddons.splice(index, 1);
        alertify.success('Added ' + name);
      }, error => {
        alertify.error(error.error.message);
      });
  }

  getInventory() {
    this.planService.getInventory().then(data => {
      this.inventory = data;
    }).catch(error => {
      alertify.error('Error fetching inventory');
    });
  }

  simAdded() {
    alertify.success('SIM added successfully');
  }

  suspendSimAction() {
    if (this.suspendSim) {
      alertify.success('Sim suspended successfully');
    } else {
      alertify.error('Invalid msisdn');
    }
  }

  recharge() {
    const data = this.rechargeForm.value;
    this.customerService.recharge(data.msisdn, parseFloat(data.quota) * 100).then(data => {
      this.rechargeForm.reset();
      alertify.success('Recharge successful');
    }).catch(error => {
      alertify.error(error.error.message || error.message);
    });
  }
  confirmBuy(type) {
    this.switchPlan = false;
    this.showPayment = true;
    this.hideBuyOption = false;
    $('.container_header')[0].scrollIntoView(true);

  }

  confirmBuyPremium(type) {
    this.switchPlan = false;
    this.showPaymentPremium = true;
    this.hideBuyOption = false;
    $('.container_header')[0].scrollIntoView(true);
  }

  confirmBuyCustom(type) {
    this.switchPlan = false;
    this.showPaymentCustom = true;
    this.hideBuyOption = false;
    $('.container_header')[0].scrollIntoView(true);
  }

  confirmBuyPlan(type) {
    this.buyPlan(type);
    this.switchPlan = false;
    this.showPayment = false;
    this.showPaymentPremium = false;
    this.showPaymentCustom = false;
  }


  async buyPlan(type) {
    let productId = null;
    switch (type) {
      case 'standard':
        productId = this.standardPlan.productId;
        break;
      case 'premium':
        productId = this.premiumPlan.productId;
        break;
      case 'custom':
        break;
    }

    if (!productId && type.toString().toLowerCase() === PLANTYPE.CUSTOM) {
      if (this.selectedDataPlanIndex < 0) {
        this.selectedDataPlanIndex = 0;
      }
      if (this.selectedVoicePlanIndex < 0 ) {
        this.selectedVoicePlanIndex = 0;
      }
      if (this.selectedSmsPlanIndex < 0 ) {
        this.selectedSmsPlanIndex = 0;
      }
      /* append all the prodcutId */
      if (this.selectedDataPlanIndex >= 0) {
        productId = this.dataPlans[this.selectedDataPlanIndex].productId;
      }
      if (this.selectedVoicePlanIndex >= 0) {
        productId = productId + '^^' + this.voicePlans[this.selectedVoicePlanIndex].productId;
      }
      if (this.selectedSmsPlanIndex >= 0) {
        productId = productId + '^^' + this.smsPlans[this.selectedSmsPlanIndex].productId;
      }
    }
    if (!productId || productId === '' || (this.selectedDataPlanIndex === 0 && this.selectedVoicePlanIndex === 0 && this.selectedSmsPlanIndex === 0)) {
      this.selectedDataPlanIndex = -1;
      this.selectedDataPlanIndex = -1;
      this.selectedDataPlanIndex = -1;
      alertify.error('Please select any of data, sms and voice');
      return false;
    }

    const requestForBuy = productId.toString().split('^^').map(x => {
      return this.customerService.buyPlan(this.msisdn, x);
    });
    const requestForRemove = this.currentPlan.productId.toString().split('^^').map(x => {
      return this.customerService.removePlan(this.msisdn, x);
    });
    try {
      const responseForBuy = await Promise.all(requestForBuy);
      const responseForRemove = await Promise.all(requestForRemove);
      const subscriptionDetail = await this.customerService.subscriptionDetail(this.msisdn);
      const currentPlan = this.utilService.getCustomerPlanFromSubscription(subscriptionDetail.products);
      this.persistenceService.set(PERSISTANCEKEY.CURRENTPLAN, currentPlan, {type: StorageType.SESSION});
      alertify.success('Plan switched successfully');
      this.getCurrentPlan();
      this.getSubscriptionDetail();
      this.switchPlan = false;
      this.planFeatures1 = [];
      this.planFeatures2 = [];
    } catch (e) {
      alertify.error('Error buying  plan');
    }
  }

  goBack() {
    this.switchPlan = false;
    this.showPayment = false;
    this.showPaymentPremium = false;
    this.showPaymentCustom = false;
    this.hideBuyOption = false;
  }

  showSubscriptionPlans() {
    this.switchPlan = true;
    this.hideBuyOption = true;
  }

  getSubscriptionDetail() {
    const msisdn = this.persistenceService.get(PERSISTANCEKEY.MSISDN, StorageType.SESSION);
    this.customerService.getSubscriptionDetail(msisdn)
      .subscribe((data: { msisdn: string, products: Product[] }) => {
        let currentPlan: Product = null;
        if (data.products) {
          currentPlan = this.utilService.getCustomerPlanFromSubscription(data.products);
          if (currentPlan) {
            if (currentPlan.productGroup === 'custom') {
              this.getCustomPlanFeature(currentPlan);
            }
          }
          this.persistenceService.set(PERSISTANCEKEY.CURRENTPLAN, currentPlan, {type: StorageType.SESSION});
          this.getCurrentPlan();
        }
      });
  }

  getCustomPlanFeature(currentPlan) {
    this.planFeatures1 = [];
    const currentCustomPlan = currentPlan;
    const customPlanFeatureArr = currentCustomPlan.productName.replace('_', ' ').replace('null^^', '').split('^^');
    this.planFeatures1 = customPlanFeatureArr;
  }
}
