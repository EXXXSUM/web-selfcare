import {AfterViewInit, Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {PersistenceService, StorageType} from 'angular-persistence';
import {FormBuilder, FormGroup} from '@angular/forms';
import {EventListenerService} from '../../event-listener.service';
import {ProductService} from '../../services/product.service';
import {CustomerService} from '../../services/customer.service';
import {PERSISTANCEKEY} from '../../../application-constants';
import {ModalType} from '../../enum/ModalType';

declare const alertify;
declare var jQuery: any;

@Component({
  selector: 'app-common-transfer-modal',
  templateUrl: './common-transfer-modal.component.html',
  styleUrls: ['./common-transfer-modal.component.scss'],
  providers: [ProductService, CustomerService]
})
export class CommonTransferModalComponent implements OnInit, OnChanges, AfterViewInit {
  @Input()
  type;
  title;
  transferFormGroup: FormGroup;
  productId;
  placeType;
  availableQuota;
  srcAccountType;
  dstAccountType;

  msisdn;
  transfer = '';
  bar = 0;
  mixCreditBalance = 0;
  maxCreditBalance = 50;
  transferUnit;

  constructor(
    private fb: FormBuilder,
    private persistenceService: PersistenceService,
    private event: EventListenerService,
    private customerService: CustomerService,
    private planService: ProductService
  ) {
    // this.transferFormGroup = this.fb.group({
    //   msisdn: ['', [Validators.required, Validators.min(0)]],
    //   transfer: ['', [Validators.required]],
    // });
  }

  customerObj = null;

  ngOnInit() {
    this.event.customerDataChangesEvent.subscribe(data => {
        this.customerObj = data;
        this.getMinMaxValuesFormTransfer();
    });
  }
  ngAfterViewInit(): void {
    jQuery('#dataTransfer').on('shown.bs.modal', (e) => {
      this.bar = 0;
      this.msisdn = '';
      this.transfer = '';
      this.getMinMaxValuesFormTransfer();
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.msisdn = '';
    this.bar = 0;
    this.transfer = '';
    // if (typeof this.transferFormGroup !== "undefined") {
    //
    // }
    if (this.type === ModalType.data) {
      this.transferUnit = 'GB';
      this.srcAccountType = 6;
      this.dstAccountType = 6;
      this.title = 'Transfer Data';
      this.productId = 'CM_Data2Share_577';
      this.placeType = '00.0 GB ';
    }
    if (this.type === ModalType.sms) {
      this.transferUnit = '';
      this.srcAccountType = 5;
      this.dstAccountType = 5;
      this.title = 'Transfer SMS';
      this.productId = 'CM_Data2Share_603';
      this.placeType = 'SMS';
    }
    if (this.type === 'voice') {
      this.transferUnit = 'Min';
      this.srcAccountType = 0;
      this.dstAccountType = 0;
      this.placeType = '00.0 Min';
      this.title = 'Transfer Minutes';
      this.productId = 'CM_Data2Share_601';
    }
    this.getMinMaxValuesFormTransfer();
  }

  getMinMaxValuesFormTransfer() {
    this.availableQuota = this.persistenceService.get(
      'availableQuota',
      StorageType.SESSION
    );

    if (this.customerObj != null && this.availableQuota) {
      this.availableQuota.data = this.customerObj.data;
      this.availableQuota.voice = this.customerObj.voice;
      this.availableQuota.sms = this.customerObj.sms;
    }
    if (this.type === ModalType.data) {
      if (this.availableQuota && this.availableQuota.data) {
        if (this.availableQuota.data === -1 || this.availableQuota.data >= 107373108658176) {
         this.maxCreditBalance = 100;
        } else {
          this.maxCreditBalance = Number.parseFloat(
          (this.availableQuota.data / (1024 * 1024 * 1024)).toFixed(2)
          );
        }
        // this.transferFormGroup.controls['transfer'].setValidators([Validators.required, Validators.min(0), Validators.max(this.availableQuota.data)])
      }
    } else if (this.type === ModalType.sms) {
      if (this.availableQuota && this.availableQuota.sms) {
        if (this.availableQuota.sms === -1 || this.availableQuota.sms >= 99999) {
          this.maxCreditBalance = 100;
       } else {
         this.maxCreditBalance = this.availableQuota.sms;
       }
        // this.transferFormGroup.controls['transfer'].setValidators([Validators.required, Validators.min(0), Validators.max(this.availableQuota.sms)])
      }
    } else if (this.type === ModalType.voice) {
      if (this.availableQuota && this.availableQuota.voice) {
        if (this.availableQuota.voice === -1 || this.availableQuota.voice >= 5999940) {
          this.maxCreditBalance = 100;
        } else {
          this.maxCreditBalance = this.availableQuota.voice / 60;
        }
        // this.transferFormGroup.controls['transfer'].setValidators([Validators.required, Validators.min(0), Validators.max(this.availableQuota.voice)])
      }
    }
  }

  transferItem(msisdn, transfer) {
    jQuery('#dataTransfer').modal('hide');
    const params: any = {};
    params.benmsisdn = msisdn;
    params.msisdn = this.persistenceService.get(PERSISTANCEKEY.MSISDN, StorageType.SESSION);
    params.transfer = transfer;
    if (this.type === ModalType.data) {
      params.transfer = transfer * 1024;
    }
    if (this.type === ModalType.voice) {
      params.transfer = transfer * 60;
    }
    if (this.type === ModalType.sms) {
      params.transfer = transfer * 1.5;
    }

    params.srcAccountType = this.srcAccountType;
    params.dstAccountType = this.dstAccountType;
    params.buyOption = 'ME2U';
    params.productId = this.productId;
    this.customerService.transferItem(params).subscribe(
      data => {
        alertify.success('Transferred Successfully');
        this.transferQuota(params.transfer);
        this.getMinMaxValuesFormTransfer();
      },
      error => {
        this.transferQuota(params.transfer);
        alertify.error(error.error.message);
      },
      () => {
      }
    );
    this.resetModal();
  }

  transferQuota(val) {
    if (this.type === ModalType.data) {
      this.event.removeData(val * 1024 * 1024);
    }
    if (this.type === ModalType.sms) {
      this.event.removeSms(val);
    }
    if (this.type === ModalType.voice) {
      this.event.removeMinutes(val);
    }
  }

  onChange(event) {
    if (typeof event !== 'undefined' && event) {
      this.transfer = event;
      this.bar = event;
    }
  }

  resetModal() {
    this.msisdn = '';
    this.bar = 0;
    this.transfer = '';
  }
}
