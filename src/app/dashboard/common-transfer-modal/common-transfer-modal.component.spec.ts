import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommonTransferModalComponent } from './common-transfer-modal.component';

describe('CommonTransferModalComponent', () => {
  let component: CommonTransferModalComponent;
  let fixture: ComponentFixture<CommonTransferModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommonTransferModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommonTransferModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
