import {AfterViewInit, Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {PersistenceService, StorageType} from 'angular-persistence';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DataClientService} from '../../data-client.service';
import {ProductService} from '../../services/product.service';
import {CustomerGroupService} from '../../services/customer-group.service';
import {CustomerService} from '../../services/customer.service';
import {PERSISTANCEKEY} from '../../../application-constants';
import {ModalType} from '../../enum/ModalType';

declare const jQuery;
declare const alertify;

@Component({
  selector: 'app-common-share-modal',
  templateUrl: './common-share-modal.component.html',
  styleUrls: ['./common-share-modal.component.scss'],
  providers: [ProductService, CustomerGroupService, CustomerService]
})
export class CommonShareModalComponent implements OnInit, OnChanges, AfterViewInit {
  @Input() type;
  title;
  availableQuota;
  shareFormGroup: FormGroup;
  msisdn;
  placeType;
  sharedUser: any[] = [];
  selectedno = '';

  constructor(private fb: FormBuilder,
              private persistenceService: PersistenceService,
              private dataService: DataClientService,
              private customerGroupService: CustomerGroupService,
              private customerService: CustomerService,
              private planService: ProductService) {
  }

  ngOnInit() {
    this.shareFormGroup = this.fb.group({
      msisdn: ['', [Validators.required, Validators.min(0)]],
      transfer: ['', [Validators.required, Validators.min(0)]],
      buyOption: 'SHRE'
    });
    this.msisdn = this.persistenceService.get(PERSISTANCEKEY.MSISDN, StorageType.SESSION);
  }

  ngAfterViewInit(): void {
    jQuery('#dataShare').on('hidden.bs.modal', (e) => {
      this.shareFormGroup.patchValue({
        msisdn: '',
        transfer: null
      });
    });
  }


  ngOnChanges(changes: SimpleChanges): void {
    this.availableQuota = this.persistenceService.get(PERSISTANCEKEY.AVAILABLEQUOTA, StorageType.SESSION);
    if (this.type === ModalType.data) {
      this.title = 'Share Data';
      this.placeType = '00.0 GB ';
    }
    if (this.type === ModalType.sms) {
      this.title = 'Share SMS';
      this.placeType = '0 SMS';
    }
    if (this.type === ModalType.voice) {
      this.placeType = '00.0 Min';
      this.title = 'Share Minutes';
    }
    if (this.type && typeof this.type !== 'undefined') {
      this.getSharedAccount();
    }
  }

  getSharedAccount() {
    this.customerGroupService.getSharedAccount(this.msisdn).then(data => {
      this.sharedUser = data.filter((x) => x.sharedQuota[this.type] && !x.removedDate);
      console.log(this.sharedUser);
    }).catch(error => {
    });
  }

  addsharedAccount(msisdno) {
    this.selectedno = msisdno;
  }

  removeSharedAccount(msisdn) {
    this.customerGroupService.removeSharedAccount(this.msisdn, msisdn)
      .subscribe(data => {
        this.getSharedAccount();
        alertify.success('removed shared account');
      }, error => {
        alertify.error(error.error.message);
      });
  }

  share(val) {
    const params: any = {};
    params.benmsisdn = val.msisdn;
    params.msisdn = this.msisdn;
    params.buyOption = val.buyOption;
    if (this.type === ModalType.data) {
      params.shareQuota = {
        data: val.transfer * 1024
      };
    }
    if (this.type === ModalType.voice) {
      params.shareQuota = {
        data: val.transfer * 60
      };
    }
    if (this.type === ModalType.sms) {
      params.shareQuota = {
        data: val.transfer
      };
    }
    this.customerService.shareQuota(params)
      .subscribe((data) => {
        alertify.success('Shared Successfully');
        this.getSharedAccount();
        jQuery('#dataShare').modal('hide');
      }, error => {
        alertify.error(error.error.message);
      }, () => {

      });
  }
}
