import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommonShareModalComponent } from './common-share-modal.component';

describe('CommonShareModalComponent', () => {
  let component: CommonShareModalComponent;
  let fixture: ComponentFixture<CommonShareModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommonShareModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommonShareModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
