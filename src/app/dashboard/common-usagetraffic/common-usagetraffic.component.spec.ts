import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommonUsagetrafficComponent } from './common-usagetraffic.component';

describe('CommonUsagetrafficComponent', () => {
  let component: CommonUsagetrafficComponent;
  let fixture: ComponentFixture<CommonUsagetrafficComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommonUsagetrafficComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommonUsagetrafficComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
