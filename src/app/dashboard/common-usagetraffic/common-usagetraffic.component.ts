import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {PersistenceService, StorageType} from 'angular-persistence';
import {ProductService} from '../../services/product.service';
import {CustomerService} from '../../services/customer.service';
import {CustomerGroupService} from '../../services/customer-group.service';
import {PERSISTANCEKEY} from '../../../application-constants';
import * as _ from 'underscore';
import * as moment from 'moment';
import {Subscription} from '../../interface/subscription';
import Product = Subscription.Product;

declare const alertify;

@Component({
  selector: 'app-common-usagetraffic',
  templateUrl: './common-usagetraffic.component.html',
  styleUrls: ['./common-usagetraffic.component.scss'],
  providers: [ProductService, CustomerService, CustomerGroupService]
})
export class CommonUsagetrafficComponent implements OnInit, OnChanges {
  @Input() type: number;
  titleSet = ['Data Usage', 'Mins Usage', 'SMS Usage', 'Credit History'];
  lowLevelTitleSet = ['Data', 'Mins', 'SMS', 'Credit'];
  currentTitle;
  currentPlan: Product;
  msisdn;

  public barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true,
    legend: {
      display: false
    }
  };
  public barChartLabels: string[] = ['Jan', 'Feb', 'Mar', 'Apr', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
  public barChartType = 'bar';
  public barChartLegend = true;

  public barChartData: any[] = [];
  colors = [];

  transfer = [];
  shared = [];
  added = [];

  constructor(private planService: ProductService,
              private persistenceService: PersistenceService,
              private customerGroupService: CustomerGroupService,
              private customerService: CustomerService) {
    this.currentTitle = this.titleSet[this.type];
    for (let i = 0; i < 11; i++) {
      this.colors.push('rgba(28, 137, 207, 1)');
    }

  }

  ngOnInit() {
    this.msisdn = this.persistenceService.get(PERSISTANCEKEY.MSISDN, StorageType.SESSION);
    if (typeof this.type !== 'undefined') {
      this.currentTitle = this.titleSet[this.type];
    }
    this.getTransferHistory();
    if (this.type === 3) {
      /* credit */
      this.getRechargeHistory(); // For added
    } else {
      this.getShared();
      this.getAdded();
    }
    this.generateData();
    this.getCurrentPlan();
  }

  ngOnChanges(changes: SimpleChanges): void {
  }

  generateData(): void {
    const data = [];
    for (let i = 0; i < 11; i++) {
      const rand = Math.floor(Math.random() * 100);
      data.push(rand);
    }
    this.barChartData = [{data: data, label: '', backgroundColor: this.colors}];
  }


  getRechargeHistory() {
    /* for added credit */
    const msisdn = this.persistenceService.get(PERSISTANCEKEY.MSISDN, StorageType.SESSION);
    this.customerService.getRechargeHistory(msisdn).then(data => {
      this.added = data.map(x => {
        x.balanceTxnDate = moment(x.balanceTxnDate, 'DD-MM-YYYY HH:mm:ss.SSS').utc().format();
        return x;
      });
      this.added = _.sortBy(this.added, obj => {
        return new Date(obj.balanceTxnDate);
      }).reverse();
      this.added = this.added.map(x => {
        x.balanceTxnDate = moment(x.balanceTxnDate).format('DD-MM-YYYY h:mm:ss');
        return x;
      });
    }).catch(error => {
      // alertify.error(error.error.message || 'credit history not available');
    });
  }

  getTransferHistory() {
    /* for all history */
    const msisdn = this.persistenceService.get(PERSISTANCEKEY.MSISDN, StorageType.SESSION);
    this.customerService.getTransferHistory(msisdn).then((data: any[]) => {
      console.log(data);
      this.transfer = data.map(x => {
        const [date, time] = x.expiryDate.toString().split(' ');
        const [d, m, y] = date.toString().split('-');
        x.expiryDate = new Date(`${y}-${m}-${d} ${time}`).toISOString();
        if (this.getTypesString(this.type) === 'sms') {
          x.transferQuota[this.getTypesString(this.type)] = Math.ceil(x.transferQuota[this.getTypesString(this.type)] / 1.5);
        }
        return x;
      }).filter(x => x.transferQuota[this.getTypesString(this.type)]);
      this.transfer = _.sortBy(this.transfer, obj => {
        return new Date(obj.expiryDate);
      }).reverse();
      this.transfer = this.transfer.map(x => {
        x.expiryDate = moment(x.expiryDate).format('DD-MM-YYYY h:mm:ss');
        return x;
      });
    }).catch(error => {
      // alertify.error(error.error.message || 'transfer history not available');
    });
  }


  getAdded() {
    const msisdn = this.persistenceService.get(PERSISTANCEKEY.MSISDN, StorageType.SESSION);
    this.customerService.subscriptionDetail(msisdn).then((data: { msisdn: string, products: Product[] }) => {
      console.log(data.products);
      this.added = data.products
        .map(x => {
          x.expiryDate = moment(x.expiryDate, 'DD-MM-YYYY HH:mm:ss').utc().format();
          return x;
        }).filter((x: Product) => {
          return x.productType.toString().toLowerCase() === this.getTypesString(this.type).toLowerCase() && x.productGroup.toLowerCase() === 'recharge';
        });
      this.added = _.sortBy(this.added, obj => {
        return new Date(obj.expiryDate);
      }).reverse();
      this.added = this.added.map(x => {
        x.expiryDate = moment(x.expiryDate).format('DD-MM-YYYY h:mm:ss');
        return x;
      });
    }).catch(error => {
      // alertify.error(error.error.message);
    });
  }

  getShared() {
    const msisdn = this.persistenceService.get(PERSISTANCEKEY.MSISDN, StorageType.SESSION);
    this.customerGroupService.getSharedAccount(msisdn).then(data => {
      this.shared = data.map(x => {
        const [date, time] = x.addedDate.toString().split(' ');
        const [y, m, d] = date.toString().split('-');
        x.expiryDate = new Date(`${y}-${m}-${d} ${time}`).toISOString();
        return x;
      }).filter((x) => x.sharedQuota[this.getTypesString(this.type)]);
      console.log(this.shared);

      this.shared = _.sortBy(this.shared, obj => {
        return new Date(obj.expiryDate);
      }).reverse();

      this.shared = this.shared.map(x => {
        x.expiryDate = moment(x.expiryDate).format('DD-MM-YYYY h:mm:ss');
        return x;
      });

    }).catch(error => {
    });
  }

  getTypesString(type: number) {
    let val = '';
    switch (type) {
      case 0:
        val = 'data';
        break;
      case 1:
        val = 'voice';
        break;
      case 2:
        val = 'sms';
        break;
      case 3:
        val = 'credit';
        break;
    }
    return val;
  }

  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }

  getCurrentPlan() {
    this.currentPlan = this.persistenceService.get(PERSISTANCEKEY.CURRENTPLAN, StorageType.SESSION);
  }
}
