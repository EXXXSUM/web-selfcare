import { Component, OnInit, Input } from '@angular/core';
import {Router} from '@angular/router';
import {ONBOARDPERSISTANCEKEY} from '../../../application-constants';
import {PersistenceService, StorageType} from 'angular-persistence';

@Component({
  selector: 'app-stepper',
  templateUrl: './stepper.component.html',
  styleUrls: ['./stepper.component.scss']
})
export class StepperComponent implements OnInit {

  @Input() step : string;
  enableRegister = false;
  enableAddOn = false;
  enableDelivery = false;
  enableMsisdn = false;
  enableCheckout = false;

  constructor(private router: Router,
    private persistenceService: PersistenceService) { }

  ngOnInit() {
    this.enableRegister = this.persistenceService.get(ONBOARDPERSISTANCEKEY.ENABLEREGISTER, StorageType.SESSION);
    this.enableDelivery = this.persistenceService.get(ONBOARDPERSISTANCEKEY.ENABLEDELIVERY, StorageType.SESSION);
    this.enableCheckout = this.persistenceService.get(ONBOARDPERSISTANCEKEY.ENABLECHECKOUT, StorageType.SESSION);
    this.enableAddOn = this.persistenceService.get(ONBOARDPERSISTANCEKEY.ENABLEADDON, StorageType.SESSION);
  }

  openRoute(route) {
    this.router.navigate(route.split('/'));
  }

}
