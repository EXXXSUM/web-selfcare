import { NgModule} from '@angular/core';
import { StepperComponent } from './stepper/stepper.component';
import { RegisterComponent } from './register/register.component';
import { DeviceComponent } from './device/device.component';
import { DeliveryComponent } from './delivery/delivery.component';
import { OnboardComponent } from './onboard.component';
import { PlanComponent } from './plan/plan.component';
import { AddonComponent } from './addon/addon.component';
import { OnboardRouting } from './onboard-routing.module';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {CustomPipeModule} from '../custom-pipe/custom-pipe.module';
import { CheckoutComponent } from './checkout/checkout.component';
import { ConfirmationComponent } from './confirmation/confirmation.component';

@NgModule({
    imports: [
        OnboardRouting,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        CustomPipeModule
    ],
    declarations: [
      StepperComponent,
      RegisterComponent,
      DeviceComponent,
      PlanComponent,
      DeliveryComponent,
      OnboardComponent,
      AddonComponent,
      CheckoutComponent,
      ConfirmationComponent
    ]
  })
export class OnboardModule {
}
