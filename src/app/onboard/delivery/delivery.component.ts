import { Component, OnInit } from '@angular/core';
import {ONBOARDPERSISTANCEKEY} from '../../../application-constants';
import {PersistenceService, StorageType} from 'angular-persistence';
import {Router} from '@angular/router';
import { PreviousRouteService } from '../../services/previous-route.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-delivery',
  templateUrl: './delivery.component.html',
  styleUrls: ['./delivery.component.scss']
})
export class DeliveryComponent implements OnInit {
  form: FormGroup;
  previousURL: string;
  deliveryMode : string;
  title = 'Home Delivery / Pickup';
  users$: Object;
  pincode : number;
  constructor(private router: Router,
    private formBuilder: FormBuilder,
    private persistenceService: PersistenceService,
    private previousRouteService: PreviousRouteService) { }
    getUsers() {
      const dataShow = [{
        'id': 1,
        'name': 'Vodafone Store Globo',
        'openingTiming': '10:00 AM',
        'closingTiming': '10:00 PM',
        'email': 'Sincere@april.biz',
        'address': 'Kulas Light Apt. 556 Gwenborough 92998-3874',
        'phone': '1-770-736-8031 x56442',
        'website': 'hildegard.org',
      },
      {
        'id': 2,
        'name': 'Vodafone Store Auro Settimo',
        'openingTiming': '10:00 AM',
        'closingTiming': '10:00 PM',
        'email': 'Sincere@april.biz',
        'address': 'Kulas Light Apt. 556 Gwenborough 92998-3874',
        'phone': '1-770-736-8031 x56442',
        'website': 'hildegard.org',
      },
      {
        'id': 3,
        'name': 'Vodafone Store Heights',
        'openingTiming': '10:00 AM',
        'closingTiming': '10:00 PM',
        'email': 'Sincere@april.biz',
        'address': 'Kulas Light Apt. 556 Gwenborough 92998-3874',
        'phone': '1-770-736-8031 x56442',
        'website': 'hildegard.org',
      },
      {
        'id': 4,
        'name': 'Vodafone Store Ghetto',
        'openingTiming': '10:00 AM',
        'closingTiming': '10:00 PM',
        'email': 'Sincere@april.biz',
        'address': 'Kulas Light Apt. 556 Gwenborough 92998-3874',
        'phone': '1-770-736-8031 x56442',
        'website': 'hildegard.org',
      }
    ];
    this.users$ = dataShow;

  }

  ngOnInit() {
    this.deliveryMode = 'homeDelivery';
    this.pincode = this.persistenceService.get(ONBOARDPERSISTANCEKEY.PINCODE, StorageType.SESSION);
    this.form = this.formBuilder.group({
      search: [null, [Validators.required]],
    });
    this.previousURL = this.previousRouteService.getPreviousUrl();
    if (this.previousURL === undefined) {
      this.router.navigate(['public']);
    }
  }

  redirectTo(route) {
    this.router.navigate(route.split('/'));
    this.persistenceService.set(ONBOARDPERSISTANCEKEY.ENABLECHECKOUT, true, { type: StorageType.SESSION });
  }
}
