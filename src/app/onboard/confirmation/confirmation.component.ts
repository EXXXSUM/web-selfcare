import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { PreviousRouteService } from '../../services/previous-route.service';

@Component({
  selector: 'app-confirmation',
  templateUrl: './confirmation.component.html',
  styleUrls: ['./confirmation.component.scss']
})
export class ConfirmationComponent implements OnInit {

  previousURL: string;

  constructor(private router: Router,
    private previousRouteService: PreviousRouteService) { }

  ngOnInit() {
    this.previousURL = this.previousRouteService.getPreviousUrl();
    if (this.previousURL === undefined) {
      this.router.navigate(['public']);
    }
  }

  redirectTo(route) {
    this.router.navigate(route.split('/'));
  }

}
