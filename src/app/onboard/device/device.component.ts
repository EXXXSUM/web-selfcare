import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ONBOARDPERSISTANCEKEY } from '../../../application-constants';
import { PersistenceService, StorageType } from 'angular-persistence';
import { PreviousRouteService } from '../../services/previous-route.service';
//import { PreviousRouteService } from '../../images/device/iPhone8.png';

@Component({
  selector: 'app-device',
  templateUrl: './device.component.html',
  styleUrls: ['./device.component.scss']
})
export class DeviceComponent implements OnInit {
  previousURL: string;
  deviceName = '';
  isDisabled = true;

  constructor(private router: Router,
    private persistenceService: PersistenceService,
    private previousRouteService: PreviousRouteService) { }
    devices = [{ 'deviceName': 'iPhone 8', 'price': 1000, 'url': 'assets/imgs/device/iPhone8.png' },
      { 'deviceName': 'iPhone 7', 'price': 950, 'url': 'assets/imgs/device/iPhone7.png' },
      { 'deviceName': 'iPhone 6', 'price': 700, 'url': 'assets/imgs/device/iPhone6.png' },
      { 'deviceName': 'Apple iPhone 6S', 'price': 500, 'url': 'assets/imgs/device/iPhone5.png' },
      { 'deviceName': 'Samsung Glaxy S9+', 'price': 820, 'url': 'assets/imgs/device/S9.png' },
      { 'deviceName': 'Samsung Glaxy S6 Edge+', 'price': 725, 'url': 'assets/imgs/device/S6.png' },
      { 'deviceName': 'Samsung Glaxy Note 9', 'price': 890, 'url': 'assets/imgs/device/Note.png' },
      { 'deviceName': 'Samsung Glaxy A8+', 'price': 429, 'url': 'assets/imgs/device/A8.png' }
    ];
  deviceSearchList: any;
  selectedDevice: any;
  ngOnInit() {
    this.deviceSearchList = this.devices;
    this.selectedDevice = this.persistenceService.get(ONBOARDPERSISTANCEKEY.SELECTEDDEVICE, StorageType.SESSION);
  }

  redirectTo(route) {
    this.router.navigate(route.split('/'));
    this.persistenceService.set(ONBOARDPERSISTANCEKEY.ENABLEADDON, true, { type: StorageType.SESSION });
  }

  searchDevice() {
    if (this.devices && this.devices.length > 0) {
      this.deviceSearchList = Object.assign([{}], this.devices);
      if (this.deviceName !== '') {
        this.deviceSearchList = this.deviceSearchList.filter(device => device.deviceName.substring(0, this.deviceName.length).toLowerCase() === this.deviceName.toLowerCase());
      }
    }
  }

  searchReset() {
    if (this.devices && this.devices.length > 0) {
      this.deviceName = '';
      this.deviceSearchList = Object.assign([{}], this.devices);
    }
  }

  getSelectedDevice(index) {
    if (this.deviceSearchList[index].deviceName === this.selectedDevice.deviceName) {
      this.isDisabled = true;
      this.selectedDevice = {};
      this.persistenceService.set(ONBOARDPERSISTANCEKEY.SELECTEDDEVICE, this.selectedDevice, {type: StorageType.SESSION});
    } else {
      this.isDisabled = false;
      this.selectedDevice = this.deviceSearchList[index];
      this.persistenceService.set(ONBOARDPERSISTANCEKEY.SELECTEDDEVICE, this.selectedDevice, {type: StorageType.SESSION});
    }
    
  }

  skipDevice(){
    this.persistenceService.set(ONBOARDPERSISTANCEKEY.SELECTEDDEVICE, {}, {type: StorageType.SESSION});
  }
}
