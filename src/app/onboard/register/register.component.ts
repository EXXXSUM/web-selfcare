import { Component, OnInit } from '@angular/core';
import {ONBOARDPERSISTANCEKEY} from '../../../application-constants';
import {PersistenceService, StorageType} from 'angular-persistence';
import {Router} from '@angular/router';
import { PreviousRouteService } from '../../services/previous-route.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {mockCountries} from '../../mockData';
import {CustomerService} from '../../services/customer.service';

declare const alertify;

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
  providers: [CustomerService]
})
export class RegisterComponent implements OnInit {
  registerFormModel: FormGroup;
  msisdn: string;
  firstName: string;
  lastName: string;
  dob: string;
  phone: number;
  emailId: string;
  addressLine1: string;
  addressLine2: string;
  city: string;
  state: string;
  country: any = {name: 'Select Country', code: 'US'};
  previousURL: string;
  inventory: string[] = [];
  phoneNumberType: string;
  pincode: number;
  password: string;
  retypePassword: string;
  mockCountry = [];
  registerdata;
  phoneNumber;

  constructor(private router: Router,
    private fb: FormBuilder,
    private customerService: CustomerService,
    private persistenceService: PersistenceService,
    private previousRouteService: PreviousRouteService) {
    this.registerFormModel = this.fb.group({
        id: '',
        firstName: ['', [Validators.required, Validators.pattern(new RegExp('[a-zA-Z0-9]'))]],
        lastName: ['', [Validators.required, Validators.pattern(new RegExp('[a-zA-Z0-9]'))]],
        email: ['', [Validators.required, Validators.email, Validators.pattern(new RegExp('[a-zA-Z0-9]'))]],
        password: ['', [Validators.required, Validators.minLength(4)]],
        phoneModel: '',
        deviceOS: '',
        registrationId: '',
        userStatus: '1',
        msisdn: ['', [Validators.required, Validators.pattern(new RegExp('[a-zA-Z0-9]'))]],
        subscriberCategory: 'POSTPAID'
      });
    }

  ngOnInit() {
    this.mockCountry = mockCountries;

    this.previousURL = this.previousRouteService.getPreviousUrl();
    if (this.previousURL === undefined) {
      this.router.navigate(['public']);
    }
    this.inventory = this.persistenceService.get(ONBOARDPERSISTANCEKEY.INVENTORY, StorageType.SESSION);

    this.phoneNumberType = 'byop';
  }

  redirectTo(route) {
    this.router.navigate(route.split('/'));
    this.persistenceService.set(ONBOARDPERSISTANCEKEY.PINCODE, this.pincode, {type: StorageType.SESSION});
    this.persistenceService.set(ONBOARDPERSISTANCEKEY.ENABLEDELIVERY, true, {type: StorageType.SESSION});
  }
  registerUser() {
    if (this.registerFormModel.controls['password'].value !== this.retypePassword) {
      alertify.error('password mismatch');
      return false;
    }
    this.registerdata = this.registerFormModel.value;
    this.msisdn = this.registerdata.msisdn;
    this.persistenceService.set(ONBOARDPERSISTANCEKEY.REGISTERDATA, this.registerFormModel.value, {type: StorageType.SESSION});
    this.persistenceService.set(ONBOARDPERSISTANCEKEY.MSISDN, this.msisdn, {type: StorageType.SESSION});
  }
  autoPopulateData(e) {
    this.emailId = 'john.smith@gmail.com';
    this.password = 'Password';
    this.retypePassword = 'Password';
    this.addressLine1 = 'Telefonaktiebolaget LM Ericsson Torshamnsgatan 21';
    this.city = 'Kista';
    this.state = 'Stockholm';
    this.country = {name: 'Sweden ', code: 'SE'};
    this.pincode = 16483;
    this.dob = '12/08/1978';
  }
  onKey(event, newValue) {
    this.msisdn = newValue;
  }

  getSelectedNumber(index){
   // this.registerdata = this.registerFormModel.value;
    this.phoneNumber = this.msisdn  = this.inventory[index];
    this.persistenceService.set(ONBOARDPERSISTANCEKEY.MSISDN, this.inventory[index], {type: StorageType.SESSION});
  }
}
