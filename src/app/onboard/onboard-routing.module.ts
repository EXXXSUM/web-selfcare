import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import { RegisterComponent } from './register/register.component';
import { DeviceComponent } from './device/device.component';
import { DeliveryComponent } from './delivery/delivery.component';
import { OnboardComponent } from './onboard.component';
import {PlanComponent} from './plan/plan.component';
import {AddonComponent} from './addon/addon.component';
import {CheckoutComponent} from './checkout/checkout.component';
import { ConfirmationComponent } from './confirmation/confirmation.component';
import {AuthGuard} from '../auth.guard';

const routes: Routes = [{
    path: '',
    component: OnboardComponent,
    children: [{
      path: 'addOn', component: AddonComponent, pathMatch: 'full'
    }, {
      path: 'device', component: DeviceComponent, pathMatch: 'full'
    }, {
      path: 'delivery', component: DeliveryComponent, pathMatch: 'full'
    }, {
      path: 'register', component: RegisterComponent, pathMatch: 'full'
    }, {
      path: 'plan', component: PlanComponent, pathMatch: 'full'
    }, {
      path: 'checkout', component: CheckoutComponent, pathMatch: 'full'
    }, {
      path: 'confirmation', component: ConfirmationComponent, pathMatch: 'full'
    }, {
      path: '', redirectTo: 'plan', pathMatch: 'full'
    }]
}];

@NgModule({
imports: [RouterModule.forChild(routes)],
exports: [RouterModule]
})

export class OnboardRouting {
}
