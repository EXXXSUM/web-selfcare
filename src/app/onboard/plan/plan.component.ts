import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {ONBOARDPERSISTANCEKEY} from '../../../application-constants';
import {PersistenceService, StorageType} from 'angular-persistence';
import {FormControl, FormGroup, Validators} from '@angular/forms';

import {ProductService} from '../../services/product.service';
import {CustomizePlanOffer} from '../../interface/product';

import {PERSISTANCEKEY, PLANTYPE} from '../../../application-constants';
import {UtilService} from '../../services/util.service';
import {CustomerService} from '../../services/customer.service';
import {CustomerGroupService} from '../../services/customer-group.service';
import {Subscription} from '../../interface/subscription';
import Product = Subscription.Product;
import { PreviousRouteService } from '../../services/previous-route.service';

@Component({
  selector: 'app-plan',
  templateUrl: './plan.component.html',
  styleUrls: ['./plan.component.scss'],
  providers: [ProductService, CustomerService]
})
export class PlanComponent implements OnInit {
  data;
  dataPlans = [];
  voicePlans = [];
  smsPlans = [];

  standardPlanOffer: CustomizePlanOffer;
  premiumPlanOffer: CustomizePlanOffer;

  selectedDataPlanIndex = 0;
  selectedVoicePlanIndex = 0;
  selectedSmsPlanIndex = 0;
  currentPlan: String;

  constructor(private router: Router,
    private persistenceService: PersistenceService,
    private utilService: UtilService,
    private customerService: CustomerService,
    private customerGroupService: CustomerGroupService,
    private planService: ProductService,
    private previousRouteService: PreviousRouteService) { }

  ngOnInit() {
    this.getCustomOffer();
    this.getPlanByType('Premium');
    this.getPlanByType('Standard');
	this.currentPlan = this.persistenceService.get(ONBOARDPERSISTANCEKEY.CURRENTPLAN, StorageType.SESSION);
  }
  getCustomOffer() {
    this.data = this.persistenceService.get(ONBOARDPERSISTANCEKEY.CUSTOMOFFER, StorageType.SESSION);
    this.dataPlans = this.data.dataPlans;
    this.smsPlans = this.data.smsPlans;
    this.voicePlans = this.data.voicePlans;
  }
  getPlanByType(type) {
      if (type.toLowerCase().trim() === PLANTYPE.STANDARD) {
        this.standardPlanOffer = this.persistenceService.get(ONBOARDPERSISTANCEKEY.STANDARD, StorageType.SESSION);
      } else if (type.toLowerCase().trim() === PLANTYPE.PREMIUM) {
        this.premiumPlanOffer = this.persistenceService.get(ONBOARDPERSISTANCEKEY.PREMIUM, StorageType.SESSION);
      }
  }
  getSumForCustom() {
    let total = 0;
    if (this.dataPlans.length > 0) {
      total = total + this.dataPlans[this.selectedDataPlanIndex].price;
    }
    if (this.voicePlans.length > 0) {
      total = total + this.voicePlans[this.selectedVoicePlanIndex].price;
    }
    if (this.smsPlans.length > 0) {
      total = total + this.smsPlans[this.selectedSmsPlanIndex].price;
    }
    return total;
  }
  selectCustomPlan(type, index) {
    switch (type) {
      case 'sms':
        this.selectedSmsPlanIndex = index;
        break;
      case 'voice':
        this.selectedVoicePlanIndex = index;
        break;
      case 'data':
        this.selectedDataPlanIndex = index;
        break;
    }
  }

  setOnboardBar(type) {
    this.persistenceService.set(ONBOARDPERSISTANCEKEY.CURRENTPLAN, type, {type: StorageType.SESSION});
  }

  redirectTo(route) {
    this.router.navigate(route.split('/'));
  }
}
