import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {PersistenceService, StorageType} from 'angular-persistence';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../services/auth.service';

import {ProductService} from '../../services/product.service';
import {CustomizePlanOffer} from '../../interface/product';

import {ONBOARDPERSISTANCEKEY, PERSISTANCEKEY, PLANTYPE} from '../../../application-constants';
import {UtilService} from '../../services/util.service';
import {CustomerService} from '../../services/customer.service';
import {CustomerGroupService} from '../../services/customer-group.service';
import {Subscription} from '../../interface/subscription';
import Product = Subscription.Product;
import { PreviousRouteService } from '../../services/previous-route.service';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss'],
  providers: [ProductService, UtilService, CustomerService]
})
export class CheckoutComponent implements OnInit {
  myplan;
  plan;
  registerdata;
  msisdn;
  addons: any;
  standard;
  premium;
  custom;
  currentPlan: Product;
  showCustom = '';
  dataPlans = [];
  voicePlans = [];
  smsPlans = [];
  device;
  saveCard = '';
  payBy = '';
  years = [2019, 2020, 2021, 2022, 2023, 2024, 2025];
  months = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];

  selectedDataPlanIndex = -1;
  selectedVoicePlanIndex = -1;
  selectedSmsPlanIndex = -1;

  standardPlan: CustomizePlanOffer;
  premiumPlan: CustomizePlanOffer;
  customdata;

  previousURL: string;

  cardName;
  cardNumber;
  cvv;
  customPrice;

  subtotalAmount = 0;
  simPrice = 10;
  planIndex;
  isApple = false;
  credit;
  debit;
  net;
  saveCards;
  


  constructor(private router: Router,
    private persistenceService: PersistenceService,
    private utilService: UtilService,
    private customerService: CustomerService,
    private customerGroupService: CustomerGroupService,
    private planService: ProductService,
    private authService: AuthService,
    private previousRouteService: PreviousRouteService) { }

  ngOnInit() {
    this.previousURL = this.previousRouteService.getPreviousUrl();
    if (this.previousURL === undefined) {
      this.router.navigate(['public']);
    }
    this.registerdata = this.persistenceService.get(ONBOARDPERSISTANCEKEY.REGISTERDATA, StorageType.SESSION);
    this.myplan = this.persistenceService.get(ONBOARDPERSISTANCEKEY.CURRENTPLAN, StorageType.SESSION);
    this.plan = this.myplan;
    this.addons = this.persistenceService.get(ONBOARDPERSISTANCEKEY.SELECTEDADDONS, StorageType.SESSION);
    this.msisdn = this.persistenceService.get(ONBOARDPERSISTANCEKEY.MSISDN, StorageType.SESSION);
    this.standard = this.persistenceService.get(ONBOARDPERSISTANCEKEY.STANDARD, StorageType.SESSION);
    this.standardPlan = this.standard;
    this.premium = this.persistenceService.get(ONBOARDPERSISTANCEKEY.PREMIUM, StorageType.SESSION);
    this.premiumPlan = this.premium;
    this.custom = this.persistenceService.get(ONBOARDPERSISTANCEKEY.CUSTOMOFFER, StorageType.SESSION);
    this.planIndex = this.persistenceService.get(ONBOARDPERSISTANCEKEY.CUSTOMPLANINDEX, StorageType.SESSION);
    this.customPrice = this.persistenceService.get(ONBOARDPERSISTANCEKEY.CUSTOMPLANPRICE, StorageType.SESSION);
    this.customdata = this.custom;
    this.getPlanByType(this.plan);
    this.getPlan();
    this.getAddons();
    this.getDevice();
    this.getTotal();
  }

  getPlan() {
    if (this.myplan === 'standard') {
      this.currentPlan = this.standard;
    } else if (this.myplan === 'premium') {
      this.currentPlan = this.premium;
    } else if (this.myplan === 'custom') {
      this.dataPlans = this.custom.dataPlans;
      this.smsPlans = this.custom.smsPlans;
      this.voicePlans = this.custom.voicePlans;
      this.showCustom = 'Custom';
    }
  }

  getAddons() {
    this.addons = this.persistenceService.get(ONBOARDPERSISTANCEKEY.SELECTEDADDONS, StorageType.SESSION);
  }

  getDevice() {
    this.device = this.persistenceService.get(ONBOARDPERSISTANCEKEY.SELECTEDDEVICE, StorageType.SESSION);
    if( this.device && this.device.deviceName && (this.device.deviceName).includes('iPhone')){
      this.isApple = true;
    }
  }
  getTotal() {
   for (const addOnItr in this.addons) {
     if (this.addons) {
    this.subtotalAmount += this.addons[addOnItr].price / 100;
  }
   }

   if (this.currentPlan) {
    this.subtotalAmount += this.currentPlan.price / 100;
   }

   this.subtotalAmount += this.customPrice / 100;

   this.subtotalAmount += (this.device && this.device.price) || 0;

   this.subtotalAmount += this.simPrice;
  }
  redirectTo(route) {
    this.router.navigate(route.split('/'));
  }
  getPlanByType(type) {
    if (type.toLowerCase().trim() === PLANTYPE.STANDARD) {
      this.standardPlan = this.persistenceService.get(ONBOARDPERSISTANCEKEY.STANDARD, StorageType.SESSION);
    } else if (type.toLowerCase().trim() === PLANTYPE.PREMIUM) {
      this.premiumPlan = this.persistenceService.get(ONBOARDPERSISTANCEKEY.PREMIUM, StorageType.SESSION);
    } else if ( type.toLowerCase().trim() === PLANTYPE.CUSTOM) {
      this.dataPlans = this.customdata.dataPlans;
      this.smsPlans = this.customdata.smsPlans;
      this.voicePlans = this.customdata.voicePlans;
    }
  }

  customerpay() {
    this.loginDummyUser();
  }

  loginDummyUser() {
    this.getDummyToken().then(() => {
     this.customersetup().then(data => {
      setTimeout(() => {
        this.customerRegister().then(data => {
          this.buyPlan(this.plan).then(data => {
            this.customeraddon().then(data => {
              this.deleteMsisdnFromInventory();
              this.router.navigate(['onboard/confirmation']);
            }).then(data => {
                this.removeDummyToken();
              }).catch(error => {
                this.deleteMsisdnFromInventory();
                this.removeDummyToken();
            });
          });
        });
      }, 3000);
      },error => {
        this.deleteMsisdnFromInventory();
        this.removeDummyToken();
      });
    });
  }



  // loginDummyUser() {
  //   this.getDummyToken().then(() => {
  //     this.customersetup();
  //   }).then(data => {
  //     this.customerRegister();
  //   }).then(data => {
  //     this.buyPlan(this.plan);
  //   }).then(data => {
  //     this.customeraddon();
  //   }).then(data => {
  //     this.deleteMsisdnFromInventory();
  //   }).then(data => {
  //     this.removeDummyToken();
  //   }).catch(error => {
  //     this.removeDummyToken();
  //   });
  // }

  async getDummyToken() {
    const res = await  this.authService.login('9824000101', 'Password').toPromise();
    if (!this.persistenceService.get(PERSISTANCEKEY.ACCESSTOKEN, StorageType.SESSION)) {
      this.persistenceService.set(PERSISTANCEKEY.ACCESSTOKEN, res.access_token, {type: StorageType.SESSION});
    } else {
      throw  new Error('access-token set');
    }
    return res.access_token;
  }

  removeDummyToken() {
    this.persistenceService.remove(PERSISTANCEKEY.ACCESSTOKEN, StorageType.SESSION);
  }

   /*               customer setup - Auth required                       */
  async customersetup() {
    const param = {
      'msisdn': this.msisdn,
      'productId': 'CM_Others_843',
      'buyOption': 'CUST'
    };
    this.customerService.addQuota(param).subscribe(data => {
      }, error => {
    });
  }
   /*                 registration - auth not required             */
  async customerRegister() {
    this.customerService.registerUser(this.registerdata)
      .subscribe((data) => {
      }, error => {
      });
  }
   /*                 Buy addons - Auth Required                 */
  async customeraddon() {
    for ( let index = 0; index < this.addons.length; index++) {
      const name = this.addons[index].productName;
      const productId = this.addons[index].productId;
      const params: any = {};
      params.msisdn = this.msisdn;
      params.productId = productId;
      params.buyOption = 'NACT';
      this.customerService.addAddon(params)
        .subscribe((data) => {
        }, error => {
      });
    }
  }
   /*                     buy Plan -Auth required                         */
  async buyPlan(type) {
    let productId = null;
    switch (type) {
      case 'standard':
        productId = this.standardPlan.productId;
        break;
      case 'premium':
        productId = this.premiumPlan.productId;
        break;
      case 'custom':
        break;
    }
    if(type === 'custom'){
      this.selectedDataPlanIndex = this.planIndex.data;
      this.selectedSmsPlanIndex = this.planIndex.sms;
      this.selectedVoicePlanIndex = this.planIndex.voice;
    }
    if (!productId && type.toString().toLowerCase() === PLANTYPE.CUSTOM) {
      if (this.selectedDataPlanIndex < 0) {
        this.selectedDataPlanIndex = 0;
      }
      if (this.selectedVoicePlanIndex < 0 ) {
        this.selectedVoicePlanIndex = 0;
      }
      if (this.selectedSmsPlanIndex < 0 ) {
        this.selectedSmsPlanIndex = 0;
      }
      /* append all the prodcutId */
      if (this.selectedDataPlanIndex >= 0) {
        productId = this.dataPlans[this.selectedDataPlanIndex].productId;
      }
      if (this.selectedVoicePlanIndex >= 0) {
        productId = productId + '^^' + this.voicePlans[this.selectedVoicePlanIndex].productId;
      }
      if (this.selectedSmsPlanIndex >= 0) {
        productId = productId + '^^' + this.smsPlans[this.selectedSmsPlanIndex].productId;
      }
    }
    if (!productId || productId === '' || (this.selectedDataPlanIndex === 0 && this.selectedVoicePlanIndex === 0 && this.selectedSmsPlanIndex === 0)) {
      this.selectedDataPlanIndex = -1;
      this.selectedVoicePlanIndex = -1;
      this.selectedSmsPlanIndex = -1;
      // alertify.error('Please select any of data, sms and voice');
     // return false;
    }

    const requestForBuy = productId.toString().split('^^').map(x => {
      return this.customerService.buyPlan(this.msisdn, x);
    });
    try {
      const responseForBuy = await Promise.all(requestForBuy);
      const subscriptionDetail = await this.customerService.subscriptionDetail(this.msisdn);
      const currentPlan = this.utilService.getCustomerPlanFromSubscription(subscriptionDetail.products);
      //this.persistenceService.set(PERSISTANCEKEY.CURRENTPLAN, currentPlan, {type: StorageType.SESSION});
    } catch (e) {
    }
  }

   /*               customer setup - Auth required                       */
  deleteMsisdnFromInventory() {
    this.customerService.removeMsisdnFromInventory(this.msisdn).subscribe((data) => {
      
    }, (error) => {
      this.removeDummyToken();
    });
  }
}
