import { Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {ONBOARDPERSISTANCEKEY} from '../../../application-constants';
import {PersistenceService, StorageType} from 'angular-persistence';
import {FormControl, FormGroup, FormBuilder} from '@angular/forms';

import {ProductService} from '../../services/product.service';
import {CustomizePlanOffer} from '../../interface/product';

import {PERSISTANCEKEY, PLANTYPE} from '../../../application-constants';
import {UtilService} from '../../services/util.service';
import {CustomerService} from '../../services/customer.service';
import {CustomerGroupService} from '../../services/customer-group.service';
import {Subscription} from '../../interface/subscription';
import Product = Subscription.Product;
import { PreviousRouteService } from '../../services/previous-route.service';

@Component({
  selector: 'app-addon',
  templateUrl: './addon.component.html',
  styleUrls: ['./addon.component.scss'],
  providers: [ProductService, CustomerService]
})
export class AddonComponent implements OnInit {
  availableAddons: CustomizePlanOffer[];
  selectedaddons: CustomizePlanOffer[] = [];
  previousURL: string;
  myGroup;
  isDisabled =  true;
  categoriesSelected = [
    false, false, false, false, false
  ];
  enableConfirmButton = false;

  constructor(private router: Router,
    private formBuilder: FormBuilder,
    private persistenceService: PersistenceService,
    private utilService: UtilService,
    private customerService: CustomerService,
    private customerGroupService: CustomerGroupService,
    private planService: ProductService,
    private previousRouteService: PreviousRouteService) {
      // this.myGroup = this.formBuilder.group({
      //   myCategory: this.formBuilder.array(this.categoriesSelected)
      // });
     }

  ngOnInit() {
    this.enableConfirmButton = false;
    this.selectedaddons = [];
    this.previousURL = this.previousRouteService.getPreviousUrl();
    if (this.previousURL === undefined) {
      this.router.navigate(['public']);
    }
    this.availableAddons = this.persistenceService.get(ONBOARDPERSISTANCEKEY.USERADDON, StorageType.SESSION);
    this.selectedaddons = this.persistenceService.get(ONBOARDPERSISTANCEKEY.SELECTEDADDONS, StorageType.SESSION);
  }
  // confirmAddAddon() {
  //   const y = this.myGroup.get('myCategory').value;
  //   for (const i in y) {
  //     if (y[i] === true) {
  //      this.selectedaddons.push(this.availableAddons[i]);
  //     }
  //   }
  //   this.persistenceService.set(ONBOARDPERSISTANCEKEY.SELECTEDADDONS, this.selectedaddons, {type: StorageType.SESSION});
  // }

  confirmAddAddon(index) {
    this.isDisabled = false; 
    this.selectedaddons.push(this.availableAddons[index]);
    this.persistenceService.set(ONBOARDPERSISTANCEKEY.SELECTEDADDONS, this.selectedaddons, {type: StorageType.SESSION});
  }

  redirectTo(route) {
    this.router.navigate(route.split('/'));
    this.persistenceService.set(ONBOARDPERSISTANCEKEY.ENABLEREGISTER, true, {type: StorageType.SESSION});
  }
//     checkSelected(category: any) {
//     this.enableConfirmButton = false;
//     this.myGroup.get('myCategory').value.forEach(x => {
//         if (x === true) {
//           this.enableConfirmButton = x;
//         }
//     });
//  }

  checkSelectedAddOn(){
    for(let i = 0 ; i<this.selectedaddons.length; i++){
    for(let j=0 ; j<this.availableAddons.length ; j++){
        if(this.selectedaddons[i]['productId'] === this.availableAddons[j]['productId']){
          this.availableAddons[j]['checked'] = true;
        }
      }    
    }
  }

  skipAddOn(){
    this.persistenceService.set(ONBOARDPERSISTANCEKEY.SELECTEDADDONS, [], {type: StorageType.SESSION});
  }
  
}
