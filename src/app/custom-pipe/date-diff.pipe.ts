import {Pipe, PipeTransform} from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'dateDiff'
})
export class DateDiffPipe implements PipeTransform {

  transform(value: string, args?: any): any {
    if (!value) {
      return '';
    }
    const expDate = value.split(' ')[0].split('-').reverse().join('-');
    const date1 = new Date(expDate);
    let date2 = new Date(new Date().setHours(0, 0, 0, 0));
    if (args) {
      date2 = new Date(args);
    }
    return moment(date1).diff(moment(date2), 'days');

  }

}
