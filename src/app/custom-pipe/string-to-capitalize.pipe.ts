import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'stringToCapitalize'
})
export class StringToCapitalizePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if (value === null) {
      return value;
    }
    const words = value.split(' ');

    return words.map(x => {
      if (x.length > 0 && isNaN(x)) {
        x = x.charAt(0).toUpperCase() + x.slice(1);
        return x;
      } else {
        return x;
      }
    }).join(' ');

  }

}
