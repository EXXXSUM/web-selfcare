import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {StringToCapitalizePipe} from './string-to-capitalize.pipe';
import {GetLocalTimePipe} from './get-local-time.pipe';
import {SetToArrayPipe} from './set-to-array.pipe';
import {ValueFormatterPipe} from './value-formatter.pipe';
import {DateDiffPipe} from './date-diff.pipe';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    StringToCapitalizePipe,
    GetLocalTimePipe,
    SetToArrayPipe,
    ValueFormatterPipe,
    DateDiffPipe
  ],
  exports: [
    StringToCapitalizePipe,
    GetLocalTimePipe,
    SetToArrayPipe,
    ValueFormatterPipe,
    DateDiffPipe
  ]
})
export class CustomPipeModule {
}
