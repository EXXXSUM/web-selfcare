import {Pipe, PipeTransform} from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'getLocalTime'
})
export class GetLocalTimePipe implements PipeTransform {

  transform(value = '', args?: any): any {
    if (value === null || value === '') {
      return '';
    }

    let format = 'DD/MM/YYYY h:mm a';
    if (args === 'day') {
      format = 'DD/MM/YYYY';
    } else if (args === 'time') {
      format = 'h:mm a';
    } else if (args === 'assist') {
      if (new Date().setHours(0, 0, 0, 0) === new Date(value).setHours(0, 0, 0, 0)) {
        /* same day */
        format = 'h:mm a';
      } else {
        /* different day */
        // format = "DD/MM/YYYY h:mm a";
        format = 'DD/MM/YYYY';
      }
    }
    return moment.utc(new Date(value)).local().format(format);
  }

}
