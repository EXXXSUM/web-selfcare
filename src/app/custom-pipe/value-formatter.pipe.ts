import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'valueFormatter'
})
export class ValueFormatterPipe implements PipeTransform {

  transform(value: any, args: string): any {
    value = parseFloat(value);
    switch (args) {
      case 'data':
        value = parseFloat((value / (1024 * 1024 * 1024)).toString()).toFixed(2);
        break;
      case 'voice':
        value = parseFloat((value / 60).toString()).toFixed(2);
        break;
      case 'price':
        value = parseFloat((value / 100).toString()).toFixed(2);
        break;
      case 'credit':
        value = parseFloat((value / 100).toString()).toFixed(2);
        break;
      case 'mb':
        value = parseFloat((value / (1024 * 1024)).toString()).toFixed(2);
        break;
      case 'mb-gb':
        value = parseFloat((value / (1024)).toString()).toFixed(2);
        break;
    }
    value = value.toString();
    const [op1, op2] = value.toString().split('.');
    if (parseInt(op2, 10) === 0) {
      value = op1.toString();
    }
    return value;
  }

}
