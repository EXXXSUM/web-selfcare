import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CisComponent} from './cis/cis.component';
import {AuthGuard} from './auth.guard';
import {OnboardComponent} from './onboard/onboard.component';

const routes: Routes = [
  {path: '', redirectTo: '/public', pathMatch: 'full'},
  {path: 'public', component: CisComponent},
  {path:  'onboard', loadChildren: 'app/onboard/onboard.module#OnboardModule', component:  OnboardComponent },
  {path: 'dashboard', loadChildren: 'app/dashboard/dashboard.module#DashboardModule', canActivate: [AuthGuard]}
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class AppRoutingModule {
}
