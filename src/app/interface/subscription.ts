import {Voice} from './voice';
import {Sms} from './sms';
import {Data} from './data';

export namespace Subscription {

  export interface ProvisionedQuota {
    voice: Voice;
    sms: Sms;
    data: Data;
  }

  export interface Other {
    unit: number;
    unitType: string;
  }

  export interface AvailableQuota {
    data: Data;
    voice: Voice;
    sms: Sms;
    other: Other;
  }

  export interface Product {
    productId: string;
    productName: string;
    productType: string;
    productGroup: string;
    price: number;
    paymentMode: string;
    srcChannel: string;
    numberOfPendingRenewals: number;
    nextRenewalDate: string;
    provisionedQuota: ProvisionedQuota;
    availableQuota: AvailableQuota;
    activationDate: string;
    expiryDate: string;
    isRecurring: boolean;
  }


  export interface AggregatedUnit {
    'unit'?: number;
    'expiryDate'?: string;
  }


}
