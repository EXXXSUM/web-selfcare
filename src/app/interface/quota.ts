import {Sms} from './sms';
import {Voice} from './voice';
import {Data} from './data';

export interface Quota {
  data: Data;
  voice: Voice;
  sms: Sms;
}
