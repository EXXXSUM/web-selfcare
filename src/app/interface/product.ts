import {Data} from './data';
import {Voice} from './voice';
import {Sms} from './sms';
import {Quota} from './quota';

export interface CustomizePlanOffer {
  productId: string;
  productName: string;
  productType: string;
  productGroup: string;
  productDescription: string;
  productValidityDays: number;
  productStatus: string;
  quota: Quota;
  price: number;
  issharedaccount: boolean;
  isSplit: boolean;
  isRecurring: boolean;
  BuyForOther: boolean;
  isSelected: boolean;
}
