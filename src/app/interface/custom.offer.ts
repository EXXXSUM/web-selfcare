export interface CustomOffer {
  dataPlans: { name: string, price: number, productId: string }[];
  voicePlans: { name: string, price: number, productId: string }[];
  smsPlans: { name: string, price: number, productId: string }[];
}
