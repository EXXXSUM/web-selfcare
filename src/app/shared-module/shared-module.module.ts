import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PaymentFormComponent} from './payment-form/payment-form.component';
import {WaterGlassComponent} from './water-glass/water-glass.component';

@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [
    PaymentFormComponent,
    WaterGlassComponent
  ],
  exports: [
    PaymentFormComponent,
    WaterGlassComponent
  ]
})
export class SharedModuleModule {
}
