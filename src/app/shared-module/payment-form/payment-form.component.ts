import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-payment-form',
  templateUrl: './payment-form.component.html',
  styleUrls: ['./payment-form.component.scss']
})
export class PaymentFormComponent implements OnInit {
  @Input() cardType = 'Credit Card';
  selectedPayment = 0;
  constructor() { }

  ngOnInit() {
  }

  selectCard(selected) {
    this.selectedPayment = selected;
  }
}
