import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';

@Component({
  selector: 'app-water-glass',
  templateUrl: './water-glass.component.html',
  styleUrls: ['./water-glass.component.scss']
})
export class WaterGlassComponent implements OnInit, OnChanges {

  @Input() provisioned = 0;
  @Input() available = 0;
  height = 0;

  constructor() {
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.provisioned < 0) {
      this.height = 100;
    } else if (this.available >= this.provisioned) {
      this.height = 100;
    } else {
      this.height = this.available / this.provisioned * 100;
    }
  }


}
