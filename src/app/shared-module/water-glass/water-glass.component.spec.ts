import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WaterGlassComponent } from './water-glass.component';

describe('WaterGlassComponent', () => {
  let component: WaterGlassComponent;
  let fixture: ComponentFixture<WaterGlassComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WaterGlassComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WaterGlassComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
